﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("Usuarios")]
    public class Usuario : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        [Column("IdUsuarioTipo")]
        public UsuarioTipoEnum Tipo { get; set; }

        public string Alias { get; set; }

        public string Contraseña { get; set; }
        [Column("IdProyecto")]
        public ProyectosEnum Proyecto { get; set; }

        public DateTime FechaAlta { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public bool Activo { get; set; }

    }
}
