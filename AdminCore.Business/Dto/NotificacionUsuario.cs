﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("NotificacionUsuarios")]
    public class NotificacionUsuario : IBaseEntity
    {
        
        public int Id { get; set; }
        public int IdNotificacion { get; set; }
        public int IdUsuario { get; set; }
        public bool Leido { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
    }
}
