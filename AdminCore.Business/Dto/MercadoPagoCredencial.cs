﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("MercadoPagoCredenciales")]
    public class MercadoPagoCredencial : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }
        [Column("IdProyecto")]
        public ProyectosEnum Proyecto { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Activo { get; set; }
    }
}
