﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{

    [Table("PromoProductos")]
    public class PromoProducto : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public int IdPromo { get; set; }

        public int IdProducto { get; set; }

        public int CantidadUnidades { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Activo { get; set; }
    }
}
