﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("ProductoSubCategorias")]
    public class SubCategoria : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public int IdCategoria { get; set; }

        public string Nombre { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Activo { get; set; }
    }
}
