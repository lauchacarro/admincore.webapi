﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("Imagenes")]
    public class Imagen : IBaseEntity
    {
        public int Id { get; set; }

        public string Link { get; set; }
        public byte[] Stream { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
    }
}
