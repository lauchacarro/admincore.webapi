﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{

    [Table("Proyectos")]
    public class Proyecto : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public string Nombre { get; set;}
        [Column("IdProyectoTipo")]
        public ProyectoTipoEnum Tipo { get; set; }

        public string GitUrl { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Activo { get; set; }
    }
}
