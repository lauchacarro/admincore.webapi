﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    public class Carrousel : IBaseEntity
    {
        public int Id { get; set; }

        public int IdImagen { get; set; }
        public string Titulo { get; set; }

        public string Descripcion { get; set; }
        [Column("IdProyecto")]
        public ProyectosEnum Proyecto { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
    }
}
