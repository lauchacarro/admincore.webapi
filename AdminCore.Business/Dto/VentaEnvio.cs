﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("VentaEnvios")]
    public class VentaEnvio : IBaseEntity
    {
        public int Id { get; set; }
        public int IdVenta { get; set; }
        
        public string Direccion { get; set; }
        public string LinkGoogleMaps { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }


    }
}
