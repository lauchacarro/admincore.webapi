﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("Promos")]
    public class Promo : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public decimal Precio { get; set; }
        [Column("IdProyecto")]
        public ProyectosEnum Proyecto { get; set; }
        [Column("IdPromoTipo")]
        public PromoTipoEnum Tipo { get; set; }

        public int? IdImagen { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Activo { get; set; }
    }
}
