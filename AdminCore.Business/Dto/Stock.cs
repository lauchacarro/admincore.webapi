﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("Stocks")]
    public class Stock : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public int IdProducto { get; set; }

        public int CantidadModificacion { get; set; }
        [Column("IdStockTipoModificacion")]
        public StockTipoModificacionEnum TipoModificacion { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Activo { get; set; }
    }
}
