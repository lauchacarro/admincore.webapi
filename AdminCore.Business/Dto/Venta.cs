﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("Ventas")]
    public class Venta : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        [Column("IdVentaEstado")]
        public VentaEstadosEnum Estado { get; set; }
        [Column("IdVentaTipo")]
        public VentaTipoEnum Tipo { get; set; }
        [Column("IdProyecto")]
        public ProyectosEnum Proyecto { get; set; }
        public DateTime FechaCambioEstado { get; set; }
        public int? IdContacto { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
    }
}
