﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("Productos")]
    public class Producto : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        [Column("IdProyecto")]
        public ProyectosEnum Proyecto { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public ProductoTipoEnum IdProductoTipo { get; set; }

        public decimal Precio { get; set; }

        public int Stock { get; set; }

        public int IdCategoria { get; set; }

        public int? IdSubCategoria { get; set; }
        public int? IdImagen { get; set; }
        public DateTime FechaAlta { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public bool Activo { get; set; }
    }
}
