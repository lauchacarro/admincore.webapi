﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Business.Dto
{
   public class BaseEntity //: IBaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public bool Active { get; set; }

        
        
    }
}
