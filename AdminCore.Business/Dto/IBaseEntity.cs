﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Business
{
    public interface IBaseEntity
    {
        [Key]
        int Id { get; set; }

        DateTime FechaAlta { get; set; }

        bool Activo { get; set; }
    }
}
