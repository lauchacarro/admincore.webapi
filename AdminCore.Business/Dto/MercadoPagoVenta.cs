﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminCore.Business.Dto
{
    [Table("MercadoPagoVentas")]
    public class MercadoPagoVenta : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int IdVenta { get; set; }
        public string idMercadoPagoPreference { get; set; }
        public int IdMercadoPagoCollector { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
    }
}
