﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AdminCore.Business.Dto
{
    public class PromoTemporal : IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public int IdPromo { get; set; }

        public DateTime FechaAlta { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaFin { get; set; }

        public bool Activo { get; set; }
    }
}
