﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum ProyectosEnum
    {
        AdminCore = 1,
        [Description("Hotel Valle Del Volcan")]
        HotelValleDelVolcan = 2,
        [Description("GoodFellas Bebidas")]
        GoodFellas = 3
    }
}
