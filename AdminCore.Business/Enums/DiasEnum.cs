﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum DiasEnum
    {
        Domingo,
        Lunes,
        Martes,
        Miércoles,
        Jueves,
        Viernes,
        Sabado
    }
}
