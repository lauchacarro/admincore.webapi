﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum VentaEstadosEnum
    {
        Reservada = 1,
        Creada ,
        PendientePagar,
        Pagada,
        PendienteEntrega,
        Finalizada,
        Devuelta,
        Cancelada


    }
}
