﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum StockTipoModificacionEnum
    {
        Venta = 1,
        Compra,
        Sobra,
        Falta
    }
}
