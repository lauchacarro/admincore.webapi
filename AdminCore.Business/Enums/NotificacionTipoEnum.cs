﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum NotificacionTipoEnum
    {
        VentaReservada = 1,
        VentaPagada,
        VentaParaEnviar
    }
}
