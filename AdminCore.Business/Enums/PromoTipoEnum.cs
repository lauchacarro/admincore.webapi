﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum PromoTipoEnum
    {
        Permanente = 1,
        Temporal
    }
}
