﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Business.Enums
{
    public enum VentaTipoEnum
    {
        Personal = 1,
        Teléfonica,
        Online,
        Correo,
        MáquinasAutomáticas
    }
}
