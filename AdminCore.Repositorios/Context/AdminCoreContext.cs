﻿using AdminCore.Business.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Repositories.Context
{
    public class AdminCoreContext : DbContext
    {




        public AdminCoreContext(DbContextOptions<AdminCoreContext> options) : base(options)
        {
            
        }

        public DbSet<Usuario> Usuarios { get; internal set; }

        public DbSet<Visita> Visitas { get; internal set; }

        public DbSet<Categoria> Categorias { get; internal set; }

        public DbSet<SubCategoria> SubCategorias { get; internal set; }

        public DbSet<Producto> Productos { get; internal set; }

        public DbSet<Promo> Promos { get; internal set; }

        public DbSet<PromoProducto> PromoProductos { get; internal set; }

        public DbSet<PromoTemporal> PromoTemporal { get; internal set; }

        public DbSet<Proyecto> Proyectos { get; internal set; }

        public DbSet<Stock> Stocks { get; internal set; }


        public DbSet<Carrousel> Carrousel { get; internal set; }

        public DbSet<MercadoPagoCredencial> MercadoPagoCredenciales { get; internal set; }

        public DbSet<MercadoPagoVenta> MercadoPagoVentas { get; internal set; }
        public DbSet<Venta> Ventas { get; internal set; }
        public DbSet<VentaProducto> VentaProductos { get; internal set; }
        public DbSet<VentaItem> VentaItems { get; internal set; }
        public DbSet<Contacto> Contactos { get; internal set; }
        public DbSet<VentaEnvio> VentaEnvios { get; internal set; }
        public DbSet<Imagen> Imagenes { get; internal set; }
        public DbSet<Notificacion> Notificaciones { get; internal set; }
        public DbSet<NotificacionUsuario> NotificacionUsuarios { get; internal set; }
    }
}
