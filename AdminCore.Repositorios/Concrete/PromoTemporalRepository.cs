﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class PromoTemporalRepository : EFRepository<PromoTemporal>, IPromoTemporalRepository
    {
        public PromoTemporalRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.PromoTemporal;
        }

        public IEnumerable<PromoTemporal> ObtenerPromosValidasEnRangoDeFechas(DateTime fechaInicio, DateTime fechaFin)
        {
            return Set.Where(x => x.FechaInicio <= fechaInicio && x.FechaFin >= fechaFin && x.Activo == true);
        }

        public PromoTemporal ObtenerPromoTemporalPorPromo(int idPromo)
        {
            return Set.FirstOrDefault(x => x.IdPromo == idPromo && x.Activo == true);
        }
    }
}
