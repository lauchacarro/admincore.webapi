﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class ProductoRepository : EFRepository<Producto>, IProductoRepository
    {
        public ProductoRepository(AdminCoreContext context): base(context)
        {
            base.Set = context.Productos;
        }

        public IEnumerable<Producto> ObtenerProductosActivosPorCategoria(int idCategoria)
        {
            return Set.Where(x => x.IdCategoria == idCategoria && x.Activo);
        }

        public IEnumerable<Producto> ObtenerProductosPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.Where(x => x.Proyecto == idProyecto && x.Activo);
        }

        public IEnumerable<Producto> ObtenerProductosActivosPorSubCategoria(int idSubCategoria)
        {
            return Set.Where(x => x.IdSubCategoria == idSubCategoria && x.Activo);
        }
    }
} 
