﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class MercadoPagoVentaRepository : EFRepository<MercadoPagoVenta>, IMercadoPagoVentaRepository
    {
        public MercadoPagoVentaRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.MercadoPagoVentas;
        }

        public MercadoPagoVenta ObtenerVentas(int idVenta)
        {
            return Set.FirstOrDefault(x => x.IdVenta == idVenta && x.Activo);
        }
    }
}
