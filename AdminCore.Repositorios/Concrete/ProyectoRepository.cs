﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class ProyectoRepository : EFRepository<Proyecto>, IProyectoRepository
    {

        public ProyectoRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Proyectos;
        }
    }
}
