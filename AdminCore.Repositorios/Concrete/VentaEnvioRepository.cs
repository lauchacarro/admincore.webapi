﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class VentaEnvioRepository : EFRepository<VentaEnvio>, IVentaEnvioRepository
    {
        public VentaEnvioRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.VentaEnvios;
        }

        public VentaEnvio ObtenerEnvioDeVenta(int idVenta)
        {
            return Set.FirstOrDefault(x => x.IdVenta == idVenta);
        }
    }
}
