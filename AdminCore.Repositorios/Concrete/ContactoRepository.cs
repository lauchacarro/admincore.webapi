﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class ContactoRepository : EFRepository<Contacto>, IContactoRepository
    {
        public ContactoRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Contactos;
        }

        public IEnumerable<Contacto> ObtenerContactosPorProyecto(ProyectosEnum idProyecto)
        {
            return base.Set.Where(x=> x.Proyecto == idProyecto && x.Activo);
        }
    }
}
