﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class UsuarioRepository : EFRepository<Usuario>, IUsuarioRepository
    {
    

        public UsuarioRepository(AdminCoreContext context) : base(context)
        {
            this.Set = context.Usuarios;
        }

        

        public Usuario GetByAlias(string alias)
        {
            return Set.FirstOrDefault(x => x.Alias == alias && x.Activo == true);
        }
    }
}
