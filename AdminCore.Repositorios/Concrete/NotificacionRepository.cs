﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class NotificacionRepository : EFRepository<Notificacion>, INotificacionRepository
    {
        public NotificacionRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Notificaciones;
        }

        public IEnumerable<Notificacion> ObtenerNotificacionesPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.Where(x=> x.Proyecto == idProyecto);
        }
    }
}
