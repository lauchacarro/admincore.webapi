﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class VentaProductoRepository : EFRepository<VentaProducto>, IVentaProductoRepository
    {
        public VentaProductoRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.VentaProductos;
        }

        public IEnumerable<VentaProducto> AgregarProductosAVenta(IEnumerable<Producto> productos, int idVenta)
        {
            List<VentaProducto> listaVentaProducto = new List<VentaProducto>();
            foreach (Producto producto in productos)
            {
                listaVentaProducto.Add(new VentaProducto()
                {
                    IdProducto = producto.Id,
                    IdVenta = idVenta,
                    FechaAlta = DateTime.Now,
                    Activo = true
                });
            }

            return base.BulkInsert(listaVentaProducto);
        }

        public IEnumerable<VentaProducto> ObtenerIdsProductosDeVenta(int idVenta)
        {
            return Set.Where(x => x.IdVenta == idVenta && x.Activo);
        }

        public IEnumerable<VentaProducto> ObtenerIdsVentas(int idProducto)
        {
            return Set.Where(x => x.IdProducto == idProducto && x.Activo);
        }

        public VentaProducto ObtenerVentaProducto(int idProducto, int idVenta)
        {
            return Set.FirstOrDefault(x => x.IdProducto == idProducto && x.IdVenta == idVenta && x.Activo);
        }
    }
}
