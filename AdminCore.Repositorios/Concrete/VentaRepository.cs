﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class VentaRepository : EFRepository<Venta>, IVentaRepository
    {
        public VentaRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Ventas;
        }

        public IEnumerable<Venta> ObtenerVentasPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.Where(x=> x.Proyecto == idProyecto && x.Activo);
        }

        public IEnumerable<Venta> ObtenerVentasPorProyecto(ProyectosEnum idProyecto, VentaEstadosEnum estado)
        {
            return Set.Where(x => x.Proyecto == idProyecto && x.Estado == estado && x.Activo);
        }

        public Venta CrearVenta(Venta venta, IEnumerable<VentaItem> items)
        {
            Venta _venta = base.Context.Ventas.Add(venta).Entity;
            foreach (VentaItem item in items)
            {
                base.Context.VentaItems.Add(item);
            }
            try
            {
                base.Context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                throw dbEx;
            }

            return _venta;
        }
    }
}
