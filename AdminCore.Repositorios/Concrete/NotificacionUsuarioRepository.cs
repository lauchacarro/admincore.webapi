﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class NotificacionUsuarioRepository : EFRepository<NotificacionUsuario>, INotificacionUsuarioRepository
    {
        public NotificacionUsuarioRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.NotificacionUsuarios;
        }
    }
}
