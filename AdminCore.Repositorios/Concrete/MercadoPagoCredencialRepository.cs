﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class MercadoPagoCredencialRepository : EFRepository<MercadoPagoCredencial>, IMercadoPagoCredencialRepository
    {

        public MercadoPagoCredencialRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.MercadoPagoCredenciales;
        }

        public MercadoPagoCredencial ObtenerCredencialPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.FirstOrDefault(x => x.Proyecto == idProyecto && x.Activo);
        }
    }
}
