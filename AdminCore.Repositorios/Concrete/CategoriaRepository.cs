﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class CategoriaRepository : EFRepository<Categoria>, ICategoriaRepository
    {

        public CategoriaRepository(AdminCoreContext context) : base(context)
        {
            this.Set = context.Categorias;
        }

        public IEnumerable<Categoria> ObtenerCategoriasPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.Where( x => x.Proyecto == idProyecto && x.Activo);
        }
    }
}
