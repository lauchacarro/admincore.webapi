﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class VentaItemRepository : EFRepository<VentaItem>, IVentaItemRepository
    {
        public VentaItemRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.VentaItems;
        }

        public IEnumerable<VentaItem> ObtenerIdsItemsDeVenta(int idVenta)
        {
            return Set.Where(x => x.IdVenta == idVenta && x.Activo);
        }

        public IEnumerable<VentaItem> ObtenerIdsVentasDeProducto(int idProducto)
        {
            return Set.Where(x => x.IdProducto == idProducto && x.Activo);
        }

        public IEnumerable<VentaItem> ObtenerIdsVentasDePromo(int idPromo)
        {
            return Set.Where(x => x.IdPromo == idPromo && x.Activo);
        }

        public VentaItem ObtenerVentaItemPorProducto(int idProducto, int idVenta)
        {
            return Set.FirstOrDefault(x => x.IdProducto == idProducto && x.IdVenta == idVenta && x.Activo);
        }

        public VentaItem ObtenerVentaItemPorPromo(int idPromo, int idVenta)
        {
            return Set.FirstOrDefault(x => x.IdPromo == idPromo && x.IdVenta == idVenta && x.Activo);
        }
    }
}
