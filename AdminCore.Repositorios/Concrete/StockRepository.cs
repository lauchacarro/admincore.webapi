﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class StockRepository : EFRepository<Stock>, IStockRepository
    {
        public StockRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Stocks;
        }
        public IEnumerable<Stock> ObtenerStockPorProducto(int idProducto)
        {
            return Set.Where(x => x.IdProducto == idProducto && x.Activo);
        }

        public int ObtenerStockTotalDeProcucto(int idProducto)
        {
            return Set.Where(x => x.IdProducto == idProducto && x.Activo && (x.TipoModificacion == StockTipoModificacionEnum.Compra || x.TipoModificacion == StockTipoModificacionEnum.Sobra)).Sum(x => x.CantidadModificacion) +
                (Set.Where(x => x.IdProducto == idProducto && x.Activo && (x.TipoModificacion == StockTipoModificacionEnum.Venta || x.TipoModificacion == StockTipoModificacionEnum.Falta)).Sum(x => x.CantidadModificacion ) * -1);
        }
    }
}
