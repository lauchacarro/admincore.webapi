﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class CarrouselRepository : EFRepository<Carrousel>, ICarrouselRepository
    {
        public CarrouselRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Carrousel;
        }

        public IEnumerable<Carrousel> ObtenerCarrouselPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.Where(x => x.Proyecto == idProyecto && x.Activo);
        }
    }
}
