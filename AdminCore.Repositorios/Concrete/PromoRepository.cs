﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Concrete
{
    public class PromoRepository : EFRepository<Promo>, IPromoRepository
    {
        public PromoRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.Promos;
        }

        public IEnumerable<Promo> ObtenerPromosPorTipo(PromoTipoEnum idPromoTipo)
        {
            return Set.Where(x => x.Tipo == idPromoTipo && x.Activo == true);
        }

        public IEnumerable<Promo> ObtenerPromosPorProyecto(ProyectosEnum idProyecto)
        {
            return Set.Where(x => x.Proyecto == idProyecto && x.Activo == true);
        }

    }
}
