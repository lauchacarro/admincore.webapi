﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class PromoProductoRepository : EFRepository<PromoProducto>, IPromoProductoRepository
    {
        public PromoProductoRepository(AdminCoreContext context) : base(context)
        {
            base.Set = context.PromoProductos;
        }

        public IEnumerable<PromoProducto> AgregarProductosAPromo(IEnumerable<Producto> productos, int idPromo)
        {
            List<PromoProducto> listaPromoProducto = new List<PromoProducto>();
            foreach (Producto producto in productos)
            {
                listaPromoProducto.Add(new PromoProducto()
                {
                    IdProducto = producto.Id,
                    IdPromo = idPromo,
                    FechaAlta = DateTime.Now,
                    Activo = true
                });
            }

            return base.BulkInsert(listaPromoProducto);
        }

        public IEnumerable<PromoProducto> ObtenerIdsProductosDePromo(int idPromo)
        {
            return Set.Where(x => x.IdPromo == idPromo && x.Activo );
        }

        public IEnumerable<PromoProducto> ObtenerIdsPromos(int idProducto)
        {
            return Set.Where(x => x.IdProducto == idProducto && x.Activo);
        }

        public PromoProducto ObtenerPromoProducto(int idProducto, int idPromo)
        {
            return Set.FirstOrDefault(x => x.IdProducto == idProducto && x.IdPromo == idPromo && x.Activo);
        }
    }
}
