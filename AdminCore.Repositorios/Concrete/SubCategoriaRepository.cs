﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class SubCategoriaRepository : EFRepository<SubCategoria>, ISubCategoriaRepository
    {
        public SubCategoriaRepository(AdminCoreContext context) : base(context)
        {
            this.Set = context.SubCategorias;
        }

        public IEnumerable<SubCategoria> ObtenerSubcategoriasPorCategoria(int idCategoria)
        {
            return Set.Where(x => x.IdCategoria == idCategoria && x.Activo == true);
        }
    }
}
