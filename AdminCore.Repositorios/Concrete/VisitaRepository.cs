﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Context;
using AdminCore.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Repositories.Concrete
{
    public class VisitaRepository : EFRepository<Visita>, IVisitaRepository
    {

        public VisitaRepository(AdminCoreContext context) : base(context)
        {
            this.Set = context.Visitas;
        }

        public IEnumerable<Visita> GetVisitasByProyect(ProyectosEnum idProyecto)
        {
            return Set.Where(x => x.Proyecto == idProyecto && x.Activo == true);
        }
    }
}
