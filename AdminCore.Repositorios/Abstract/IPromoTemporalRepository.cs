﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IPromoTemporalRepository : IRepository<PromoTemporal>
    {
        IEnumerable<PromoTemporal> ObtenerPromosValidasEnRangoDeFechas(DateTime fechaInicio, DateTime fechaFin);

        PromoTemporal ObtenerPromoTemporalPorPromo(int idPromo);
    }
}
