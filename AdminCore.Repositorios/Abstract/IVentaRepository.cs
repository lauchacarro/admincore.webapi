﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IVentaRepository : IRepository<Venta>
    {
        IEnumerable<Venta> ObtenerVentasPorProyecto(ProyectosEnum idProyecto);

        IEnumerable<Venta> ObtenerVentasPorProyecto(ProyectosEnum idProyecto, VentaEstadosEnum estado);

        Venta CrearVenta(Venta venta, IEnumerable<VentaItem> items);
    }
}
