﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface ICarrouselRepository : IRepository<Carrousel>
    {
        IEnumerable<Carrousel> ObtenerCarrouselPorProyecto(ProyectosEnum idProyecto);

    }
}
