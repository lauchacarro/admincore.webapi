﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface INotificacionUsuarioRepository : IRepository<NotificacionUsuario>
    {
    }
}
