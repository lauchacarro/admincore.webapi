﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IStockRepository : IRepository<Stock>
    {
        IEnumerable<Stock> ObtenerStockPorProducto(int idProducto);

        int ObtenerStockTotalDeProcucto(int idProducto);
    }
}
