﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface INotificacionRepository : IRepository<Notificacion>
    {
        IEnumerable<Notificacion> ObtenerNotificacionesPorProyecto(ProyectosEnum idProyecto);
    }
}
