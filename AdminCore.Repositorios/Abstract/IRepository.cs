﻿using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Repositories.Abstract
{
    public interface IRepository<T>
    {
        T Insert(T entity);

        T Update(T entity);

        void Delete(T entity);

        void DeleteRange(IQueryable<T> entities);

        IQueryable<T> GetAll();

        IQueryable<T> GetAllActive();

        T GetById(int id);

        IEnumerable<T> BulkInsert(IEnumerable<T> entities);
    }
}