﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IPromoProductoRepository : IRepository<PromoProducto>
    {
        PromoProducto ObtenerPromoProducto(int idProducto, int idPromo);

        IEnumerable<PromoProducto> AgregarProductosAPromo(IEnumerable<Producto> productos, int idPromo);

        IEnumerable<PromoProducto> ObtenerIdsProductosDePromo(int idPromo);

        IEnumerable<PromoProducto> ObtenerIdsPromos(int idProducto);
    }
}
