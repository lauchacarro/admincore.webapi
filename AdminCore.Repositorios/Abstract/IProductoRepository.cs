﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IProductoRepository : IRepository<Producto>
    {
        IEnumerable<Producto> ObtenerProductosPorProyecto(ProyectosEnum idProyecto);

        IEnumerable<Producto> ObtenerProductosActivosPorCategoria(int idCategoria);

        IEnumerable<Producto> ObtenerProductosActivosPorSubCategoria(int idSubCategoria);
    }
}
