﻿
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IVisitaRepository : IRepository<Business.Dto.Visita>
    {
        IEnumerable<Business.Dto.Visita> GetVisitasByProyect(ProyectosEnum idProyecto);
    }
}
