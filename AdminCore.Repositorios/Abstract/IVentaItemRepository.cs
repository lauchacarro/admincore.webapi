﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IVentaItemRepository : IRepository<VentaItem>
    {
        IEnumerable<VentaItem> ObtenerIdsItemsDeVenta(int idVenta);

        IEnumerable<VentaItem> ObtenerIdsVentasDeProducto(int idProducto);

        IEnumerable<VentaItem> ObtenerIdsVentasDePromo(int idPromo);

        VentaItem ObtenerVentaItemPorPromo(int idPromo, int idVenta);

        VentaItem ObtenerVentaItemPorProducto(int idProducto, int idVenta);
    }
}
