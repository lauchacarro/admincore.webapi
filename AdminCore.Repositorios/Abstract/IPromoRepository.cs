﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IPromoRepository : IRepository<Promo>
    {
        IEnumerable<Promo> ObtenerPromosPorTipo(PromoTipoEnum idPromoTipo);

        IEnumerable<Promo> ObtenerPromosPorProyecto(ProyectosEnum idProyecto);
    }
}
