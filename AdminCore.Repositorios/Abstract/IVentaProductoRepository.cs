﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Repositories.Abstract
{
    public interface IVentaProductoRepository : IRepository<VentaProducto>
    {
        VentaProducto ObtenerVentaProducto(int idProducto, int idVenta);

        IEnumerable<VentaProducto> AgregarProductosAVenta(IEnumerable<Producto> productos, int idVenta);

        IEnumerable<VentaProducto> ObtenerIdsProductosDeVenta(int idVenta);

        IEnumerable<VentaProducto> ObtenerIdsVentas(int idProducto);
    }
}
