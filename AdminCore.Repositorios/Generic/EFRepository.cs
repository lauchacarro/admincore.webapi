﻿
using AdminCore.Business;
using AdminCore.Repositories.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Repositories.Generic
{
    public class EFRepository<T> where T : class, IBaseEntity 
    {


        public virtual AdminCoreContext Context { get; set; }

        public virtual DbSet<T> Set { get; set; }

        public EFRepository(AdminCoreContext context)
        {
            Context = context;
        }



        public T GetById(int id)
        {
            return Set.FirstOrDefault(x => x.Id == id);
        }


        public virtual T Insert(T entity)
        {
            T savedEntity = Set.Add(entity).Entity;

            try
            {

                this.Context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                //Exception raise = (from validationErrors in dbEx.EntityValidationErrors
                //                   from validationError in validationErrors.ValidationErrors
                //                   select $"{validationErrors.Entry.Entity}:{validationError.ErrorMessage}").Aggregate<string, Exception>(dbEx, (current, message) => new InvalidOperationException(message, current));
                throw dbEx;
            }

            return savedEntity;
        }

        public virtual T Update(T entity)
        {
            this.Context.Entry(entity).State = EntityState.Modified;
            this.Context.SaveChanges();
            return entity;
        }

        public virtual void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            this.Set.Attach(entity);
            this.Set.Remove(entity);
            this.Context.SaveChanges();
        }

        public virtual void DeleteRange(IQueryable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            this.Set.RemoveRange(entities);
            this.Context.SaveChanges();
        }

        public virtual IQueryable<T> GetAll()
        {
            return this.Set;
        }

        public virtual IQueryable<T> GetAllActive()
        {
            return this.Set.Where(x => x.Activo == true);
        }

        public virtual IEnumerable<T> BulkInsert(IEnumerable<T> entities)
        {
            List<T> savedEntities = new List<T>();

            foreach (T entity in entities)
            {
                savedEntities.Add(this.Set.Add(entity).Entity);
            }
            try
            {
                this.Context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                //Exception raise = (from validationErrors in dbEx.EntityValidationErrors
                //                   from validationError in validationErrors.ValidationErrors
                //                   select $"{validationErrors.Entry.Entity}:{validationError.ErrorMessage}").Aggregate<string, Exception>(dbEx, (current, message) => new InvalidOperationException(message, current));
                throw dbEx;
            }

            return savedEntities;

        }
    }
}
