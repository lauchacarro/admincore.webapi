﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace AdminCore.Services.Utilities
{
    public static class PasswordHasher
    {
        public static string HashPassword(string phrase)
        {
            var encoder = new UTF8Encoding();
            var hasher = new SHA1CryptoServiceProvider();
            var hashedDataBytes = hasher.ComputeHash(encoder.GetBytes(phrase));
            var result = new StringBuilder();

            foreach (var t in hashedDataBytes)
            {
                result.Append(t.ToString("X2"));
            }

            return result.ToString();
        }

        public static string HashPassword(string providedPassword, string providedUsername)
        {
            var usPas = $"{providedUsername.ToLower()}{providedPassword}";
            return HashPassword(usPas);
        }

        public static bool VerifyHashedPassword(string hashedPassword, string providedPassword, string providedUsername)
        {
            // Get the password from the Database; it is hashed
            // Hash the provided password
            // Compare the DB password to the provided password
            var usPas = $"{providedUsername.ToLower()}{providedPassword}";
            var hashIt = HashPassword(usPas);

            return hashedPassword.Equals(hashIt);
        }
    }
}
