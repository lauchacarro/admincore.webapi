﻿using AdminCore.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Services.Validation
{
    public class ServiceResponseEnumerableResult<T> : ServiceResponse where T : IBaseEntity
    {
        private IEnumerable<T> _result;

        public List<T> Result { get => _result.ToList(); set => _result = value; }

        public ServiceResponseEnumerableResult(IEnumerable<T> result) : base(true, null)
        {
            _result = result;
        }

        public ServiceResponseEnumerableResult(bool success, string mensage) : base(success, mensage)
        {

        }
    }
}
