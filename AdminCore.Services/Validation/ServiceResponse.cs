﻿using AdminCore.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Validation
{
    public class ServiceResponse
    {
        public bool Success { get; set; }

        public string Mensage { get; set; }

        public object Result { get; set; }

        public ServiceResponse(object result) : this(true, null)
        {
            Result = result;
        }

        public ServiceResponse(bool success, string mensage)
        {
            Success = success;
            Mensage = mensage;
        }

    }
}
