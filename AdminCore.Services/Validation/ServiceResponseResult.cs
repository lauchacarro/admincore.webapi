﻿using AdminCore.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Validation
{
    public class ServiceResponseResult<T> : ServiceResponse where T : IBaseEntity
    {
        public T Result { get; set; }

        public ServiceResponseResult(T result) : base(true, null)
        {
            Result = result;
        }

        public ServiceResponseResult(bool success, string mensage) : base(success, mensage)
        {

        }
    }
}
