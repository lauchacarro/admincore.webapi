﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class VentaProductoService : IVentaProductoService
    {
        private readonly IVentaProductoRepository _repository;

        public VentaProductoService(IVentaProductoRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseEnumerableResult<VentaProducto> AgregarProductosAVenta(IEnumerable<Producto> productos, int idVenta)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaProducto>(_repository.AgregarProductosAVenta(productos, idVenta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaProducto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaProducto> AgregarVentaProducto(VentaProducto ventaProducto)
        {
            try
            {
                return new ServiceResponseResult<VentaProducto>(_repository.Insert(ventaProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaProducto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaProducto> ModificarVentaProducto(VentaProducto ventaProducto)
        {
            try
            {
                return new ServiceResponseResult<VentaProducto>(_repository.Update(ventaProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaProducto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaProducto> ObtenerIdsProductosDeVenta(int idVenta)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaProducto>(_repository.ObtenerIdsProductosDeVenta(idVenta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaProducto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaProducto> ObtenerIdsVentas(int idProducto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaProducto>(_repository.ObtenerIdsVentas(idProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaProducto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaProducto> QuitarProductoDeVenta(int idProducto, int idVenta)
        {
            try
            {
                VentaProducto ventaProducto = _repository.ObtenerVentaProducto(idProducto, idVenta);
                ventaProducto.Activo = false;
                return new ServiceResponseResult<VentaProducto>(_repository.Update(ventaProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaProducto>(false, ex.Message);
            }
        }
    }
}
