﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class CarrouselService : ICarrouselService
    {
        readonly ICarrouselRepository _repository;

        public CarrouselService(ICarrouselRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseResult<Carrousel> AgregarCarrousel(Carrousel carrousel)
        {
            try
            {
                return new ServiceResponseResult<Carrousel>(_repository.Insert(carrousel));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Carrousel>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Carrousel> DesactivarCarrousel(int idCarrousel)
        {
            Carrousel carrousel = _repository.GetById(idCarrousel);
            carrousel.Activo = false;
            return ModificarCarrousel(carrousel);
        }

        public ServiceResponseResult<Carrousel> ModificarCarrousel(Carrousel carrousel)
        {
            try
            {
                return new ServiceResponseResult<Carrousel>(_repository.Update(carrousel));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Carrousel>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Carrousel> ObtenerCarrouseles(ProyectosEnum proyecto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Carrousel>(_repository.ObtenerCarrouselPorProyecto(proyecto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Carrousel>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Carrousel> ObtenerCarrouselPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<Carrousel>(_repository.GetById(id));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Carrousel>(false, ex.Message);
            }
        }
    }
}
