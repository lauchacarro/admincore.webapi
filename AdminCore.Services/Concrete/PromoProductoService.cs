﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;


namespace AdminCore.Services.Concrete
{
    public class PromoProductoService : IPromoProductoService
    {
        readonly IPromoProductoRepository _promoProductoRepository;

        public PromoProductoService(IPromoProductoRepository promoProductoRepository)
        {
            _promoProductoRepository = promoProductoRepository;
        }

        public ServiceResponseEnumerableResult<PromoProducto> AgregarProductosAPromo(IEnumerable<Producto> productos, int idPromo)
        {
            try
            {
                return new ServiceResponseEnumerableResult<PromoProducto>(_promoProductoRepository.AgregarProductosAPromo(productos, idPromo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<PromoProducto>(false, ex.Message);
            }

        }

        public ServiceResponseResult<PromoProducto> AgregarPromoProducto(PromoProducto promoProducto)
        {
            try
            {
                return new ServiceResponseResult<PromoProducto>(_promoProductoRepository.Insert(promoProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoProducto>(false, ex.Message);
            }
        }



        public ServiceResponseResult<PromoProducto> ModificarPromoProducto(PromoProducto promoProducto)
        {
            try
            {
                return new ServiceResponseResult<PromoProducto>(_promoProductoRepository.Update(promoProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoProducto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<PromoProducto> ObtenerIdsProductosDePromo(int idPromo)
        {
            try
            {
                return new ServiceResponseEnumerableResult<PromoProducto>(_promoProductoRepository.ObtenerIdsProductosDePromo(idPromo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<PromoProducto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<PromoProducto> ObtenerIdsPromos(int idProducto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<PromoProducto>(_promoProductoRepository.ObtenerIdsPromos(idProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<PromoProducto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<PromoProducto> QuitarProductoDePromo(int idProducto, int idPromo)
        {
            try
            {
                PromoProducto promoProducto = _promoProductoRepository.ObtenerPromoProducto(idProducto, idPromo);
                promoProducto.Activo = false;
                return new ServiceResponseResult<PromoProducto>(_promoProductoRepository.Update(promoProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoProducto>(false, ex.Message);
            }
        }
    }
}
