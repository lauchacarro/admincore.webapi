﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Services.Concrete
{
    public class SubCategoriaService : ISubCategoriaService
    {
        readonly ISubCategoriaRepository _repository;
        readonly IProductoService _productoService;
        public SubCategoriaService(ISubCategoriaRepository repository, IProductoService productoService)
        {
            _repository = repository;
            _productoService = productoService;
        }

        public ServiceResponseResult<SubCategoria> AgregarSubCategoria(SubCategoria subCategoria)
        {
            try
            {
                return new ServiceResponseResult<SubCategoria>(_repository.Insert(subCategoria) );
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<SubCategoria>(false, ex.Message);
            }
        }

        public ServiceResponseResult<SubCategoria> DesactivarSubCategoria(int id)
        {
            List<Producto> productos = _productoService.ObtenerProductosPorSubCategoria(id).Result;
            if (productos.Count > 0)
            {
                return new ServiceResponseResult<SubCategoria>(false,  "No se puede eliminar una SubCategoria que contengan productos activos." );
            }
            SubCategoria subCategoria = _repository.GetById(id);
            subCategoria.Activo = false;
            return ModificarSubCategoria(subCategoria);
        }

        public ServiceResponseResult<SubCategoria> ModificarSubCategoria(SubCategoria subCategoria)
        {
            try
            {
                return new ServiceResponseResult<SubCategoria>(_repository.Update(subCategoria));
          
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<SubCategoria>(false, ex.Message);
            }
           
        }

        public ServiceResponseResult<SubCategoria> ObtenerSubCategoriaPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<SubCategoria>(_repository.GetById(id) );
            }
            catch (Exception ex )
            {

                return new ServiceResponseResult<SubCategoria>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<SubCategoria> ObtenerSubCategorias(int idCategoria)
        {
            try
            {
                return new ServiceResponseEnumerableResult<SubCategoria>(_repository.ObtenerSubcategoriasPorCategoria(idCategoria) );
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<SubCategoria>(false, ex.Message);
            }
        }
    }
}
