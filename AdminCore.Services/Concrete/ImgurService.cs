﻿using AdminCore.Services.Abstract;
using Imgur.API;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Services.Concrete
{

    public class ImgurService : IImgurService
    {
        public ImgurService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        IConfiguration Configuration { get; }
        public IImage ObtenerImagen(string idImageImgur)
        {
            try
            {
                return GetImageEndpoint().GetImageAsync(idImageImgur).GetAwaiter().GetResult();
            }
            catch (ImgurException imgurEx)
            {
                return null;
            }
        }

        public async Task<IImage> ObtenerImagenAsync(string idImageImgur)
        {
            try
            {
                return await GetImageEndpoint().GetImageAsync(idImageImgur); ;
            }
            catch (ImgurException imgurEx)
            {
                return null;
            }
        }

        public IImage SubirImagen(byte[] imageByte)
        {
            try
            {
                return GetImageEndpoint().UploadImageBinaryAsync(imageByte).GetAwaiter().GetResult();
            }
            catch (ImgurException imgurEx)
            {
                return null;
            }
        }

        public async Task<IImage> SubirImagenAsync(byte[] imageByte)
        {
            try
            {
                return await GetImageEndpoint().UploadImageBinaryAsync(imageByte);
            }
            catch (ImgurException imgurEx)
            {
                return null;
            }
        }

        private ImageEndpoint GetImageEndpoint()
        {
            ImgurClient client = new ImgurClient(Configuration["Settings:ImgurClientId"], Configuration["Settings:ImgurClientSecrete"]);
            return new ImageEndpoint(client);
        }
    }
}
