﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Services.Concrete
{
    public class VentaItemService : IVentaItemService
    {

        private readonly IVentaItemRepository _repository;

        public VentaItemService(IVentaItemRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseEnumerableResult<VentaItem> AgregarProductosAVenta(IEnumerable<Producto> productos, int idVenta)
        {
            try
            {
                List<VentaItem> listaVentaItem = new List<VentaItem>();
                //Se hace foreach de la agrupacion de los productos por Id
                //Luego en la asignacion de CantidadUnidades se hace el count del Id
                foreach (Producto producto in productos.GroupBy(o => o.Id).Select(g => g.First()))
                {
                    listaVentaItem.Add(new VentaItem()
                    {
                        CantidadUnidades = productos.Count(x=> x.Id == producto.Id),
                        IdVenta = idVenta,
                        IdProducto = producto.Id,
                        IdPromo = null,
                        FechaAlta = DateTime.Now,
                        Activo = true
                    });
                }
                return new ServiceResponseEnumerableResult<VentaItem>(_repository.BulkInsert(listaVentaItem));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaItem> AgregarPromosAVenta(IEnumerable<Promo> promos, int idVenta)
        {
            try
            {
                List<VentaItem> listaVentaItem = new List<VentaItem>();
                //Se hace foreach de la agrupacion de los Promos por Id
                //Luego en la asignacion de CantidadUnidades se hace el count del Id
                foreach (Promo promo in promos.GroupBy(o => o.Id).Select(g => g.First()))
                {
                    listaVentaItem.Add(new VentaItem()
                    {
                        CantidadUnidades = promos.Count(x => x.Id == promo.Id),
                        IdVenta = idVenta,
                        IdProducto = null,
                        IdPromo = promo.Id,
                        FechaAlta = DateTime.Now,
                        Activo = true
                    });
                }
                return new ServiceResponseEnumerableResult<VentaItem>(_repository.BulkInsert(listaVentaItem));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaItem> AgregarVentaItem(VentaItem ventaItem)
        {
            try
            {
                return new ServiceResponseResult<VentaItem>(_repository.Insert(ventaItem));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaItem> AgregarVentaItems(IEnumerable<VentaItem> ventaItems)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaItem>(_repository.BulkInsert(ventaItems));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaItem> ObtenerIdsItemsDeVenta(int idVenta)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaItem>(_repository.ObtenerIdsItemsDeVenta(idVenta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaItem> ObtenerIdsVentasDeProducto(int idProducto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaItem>(_repository.ObtenerIdsVentasDeProducto(idProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<VentaItem> ObtenerIdsVentasDePromo(int idPromo)
        {
            try
            {
                return new ServiceResponseEnumerableResult<VentaItem>(_repository.ObtenerIdsVentasDePromo(idPromo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaItem> QuitarProductoDeVenta(int idProducto, int idVenta)
        {
            try
            {
                VentaItem ventaItem = _repository.ObtenerVentaItemPorProducto(idProducto, idVenta);
                ventaItem.Activo = false;
                return new ServiceResponseResult<VentaItem>(_repository.Update(ventaItem));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaItem> QuitarPromoDeVenta(int idPromo, int idVenta)
        {
            try
            {
                VentaItem ventaItem = _repository.ObtenerVentaItemPorPromo(idPromo, idVenta);
                ventaItem.Activo = false;
                return new ServiceResponseResult<VentaItem>(_repository.Update(ventaItem));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaItem>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaItem> QuitarVentaItem(int id)
        {
            throw new NotImplementedException();
        }
    }
}
