﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Services.Concrete
{
    public class PromoService : IPromoService
    {
        readonly IPromoRepository _promoRepository;
        readonly IPromoTemporalService _promoTemporalService;
        readonly IPromoProductoService _promoProductoService;

        public PromoService(IPromoRepository promoRepository, IPromoTemporalService promoTemporalService, IPromoProductoService promoProductoService)
        {
            _promoRepository = promoRepository;
            _promoTemporalService = promoTemporalService;
            _promoProductoService = promoProductoService;
        }

        public ServiceResponseResult<Promo> AgregarPromo(Promo promo)
        {

            try
            {
                return new ServiceResponseResult<Promo>(_promoRepository.Insert(promo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Promo>(false, ex.Message);
            }
        }



        public ServiceResponseResult<Promo> DesactivarPromo(int idPromo)
        {

            Promo promo = _promoRepository.GetById(idPromo);
            List<PromoProducto> listaPromoProducto = _promoProductoService.ObtenerIdsProductosDePromo(idPromo).Result;
            PromoTemporal promoTemporal = _promoTemporalService.ObtenerPromoTemporalPorPromo(idPromo).Result;

            if(promoTemporal != null)
            {
                promoTemporal.Activo = false;
                _promoTemporalService.ModificarPromoTemporal(promoTemporal);
            }

            

            foreach (PromoProducto promoProducto in listaPromoProducto)
            {
                promoProducto.Activo = false;
                _promoProductoService.ModificarPromoProducto(promoProducto);
            }

            promo.Activo = false;
            return ModificarPromo(promo);
        }


        public ServiceResponseResult<Promo> ModificarPromo(Promo promo)
        {
            try
            {
                return new ServiceResponseResult<Promo>(_promoRepository.Update(promo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Promo>(false, ex.Message);
            }

        }





        public ServiceResponseResult<Promo> ObtenerPromoPorId(int idPromo)
        {
            try
            {
                return new ServiceResponseResult<Promo>(_promoRepository.GetById(idPromo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Promo>(false, ex.Message);
            }

        }

        public ServiceResponseEnumerableResult<Promo> ObtenerPromos(int idProducto)
        {

            try
            {
                List<Promo> listaPromos = new List<Promo>();

                IEnumerable<PromoProducto> listaPromoProducto = _promoProductoService.ObtenerIdsPromos(idProducto).Result;

                foreach (PromoProducto promo in listaPromoProducto)
                {
                    listaPromos.Add(_promoRepository.GetById(promo.Id));
                }
                return new ServiceResponseEnumerableResult<Promo>(listaPromos);
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Promo>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Promo> ObtenerPromos(ProyectosEnum idProyecto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Promo>(_promoRepository.ObtenerPromosPorProyecto(idProyecto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Promo>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Promo> ObtenerPromosPorTipo(PromoTipoEnum promoTipo)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Promo>(_promoRepository.ObtenerPromosPorTipo(promoTipo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Promo>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Promo> ObtenerPromosValidasHoy()
        {


            try
            {
                List<Promo> listaPromos = new List<Promo>();

                List<PromoTemporal> listaPromosTemporales = _promoTemporalService.ObtenerPromosValidasEnRangoDeFechas(DateTime.Now, DateTime.Now.AddHours(1)).Result;

                foreach (PromoTemporal promoTemporal in listaPromosTemporales)
                {
                    listaPromos.Add(_promoRepository.GetById(promoTemporal.IdPromo));
                }

                return new ServiceResponseEnumerableResult<Promo>(listaPromos);
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Promo>(false, ex.Message);
            }
        }




    }
}
