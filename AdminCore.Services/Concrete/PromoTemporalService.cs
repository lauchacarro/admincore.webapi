﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;


namespace AdminCore.Services.Concrete
{
    public class PromoTemporalService : IPromoTemporalService
    {
        readonly IPromoTemporalRepository _promoTemporalRepository;

        public PromoTemporalService(IPromoTemporalRepository promoTemporalRepository)
        {
            _promoTemporalRepository = promoTemporalRepository;
        }

        public ServiceResponseResult<PromoTemporal> AgregarPromoTemporal(PromoTemporal promoTemporal)
        {
            try
            {
                return new ServiceResponseResult<PromoTemporal>(_promoTemporalRepository.Insert(promoTemporal));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoTemporal>(false, ex.Message);
            }
        }

        public ServiceResponseResult<PromoTemporal> ModificarPromoTemporal(PromoTemporal promoTemporal)
        {
            try
            {
                return new ServiceResponseResult<PromoTemporal>(_promoTemporalRepository.Update(promoTemporal));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoTemporal>(false, ex.Message);
            }

        }

        public ServiceResponseEnumerableResult<PromoTemporal> ObtenerPromosValidasEnRangoDeFechas(DateTime fechaDesde, DateTime fechaHasta)
        {
            try
            {
                return new ServiceResponseEnumerableResult<PromoTemporal>(_promoTemporalRepository.ObtenerPromosValidasEnRangoDeFechas(fechaDesde, fechaHasta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<PromoTemporal>(false, ex.Message);
            }
        }

        public ServiceResponseResult<PromoTemporal> ObtenerPromoTemporalPorId(int idPromoTemporal)
        {
            try
            {
                return new ServiceResponseResult<PromoTemporal>(_promoTemporalRepository.GetById(idPromoTemporal));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoTemporal>(false, ex.Message);
            }

        }

        public ServiceResponseResult<PromoTemporal> ObtenerPromoTemporalPorPromo(int idPromo)
        {
            try
            {
                return new ServiceResponseResult<PromoTemporal>(_promoTemporalRepository.ObtenerPromoTemporalPorPromo(idPromo));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<PromoTemporal>(false, ex.Message);
            }
        }
    }
}
