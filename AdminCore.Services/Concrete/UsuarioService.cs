﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using AdminCore.Services.Configuration;
using AdminCore.Services.Validation;
using System;

namespace AdminCore.Services.Concrete
{
    public class UsuarioService : IUsuarioService
    {

        private readonly IUsuarioRepository _repository;

        private readonly Settings _settings;

        public UsuarioService(IOptions<Settings> settings, IUsuarioRepository repository)
        {
            _repository = repository;
            _settings = settings.Value;
        }


        public ServiceResponseResult<Usuario> ObtenerPorId(int id)
        {
            try
            {
                Usuario usuario = _repository.GetById(id);
                usuario.Contraseña = Encryption.Decrypt(usuario.Contraseña, _settings.SecretKey);
                return new ServiceResponseResult<Usuario>(usuario);
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Usuario>(false, ex.Message);
            }


        }

        public bool EsValido(ref Usuario usuario, out string msgError)
        {
            msgError = null;
            Usuario _usuario = _repository.GetByAlias(usuario.Alias);
            if (_usuario != null)
            {
                _usuario.Contraseña = Encryption.Decrypt(_usuario.Contraseña, _settings.SecretKey);
            }

            if (_usuario == null || _usuario.Contraseña != usuario.Contraseña)
            {
                msgError = "El usuario o contraseña son incorrectos.";
                return false;
            }
            usuario.Contraseña = null;
            usuario = _usuario;
            return true;
        }

        public ServiceResponseResult<Usuario> AgregarUsuario(Usuario usuario)
        {
            try
            {
                usuario.Contraseña = Encryption.Encrypt(usuario.Contraseña, _settings.SecretKey);

                return new ServiceResponseResult<Usuario>(_repository.Insert(usuario));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Usuario>(false, ex.Message);
            }

        }

        public ServiceResponseResult<Usuario> ModificarUsuario(Usuario usuario)
        {
            try
            {
                return new ServiceResponseResult<Usuario>(_repository.Update(usuario));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Usuario>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Usuario> DesactivarUsuario(int id)
        {

            Usuario usuario = _repository.GetById(id);
            usuario.Activo = false;
            return ModificarUsuario(usuario);
        }

        public ServiceResponseResult<Usuario> CambiarContraseñaUsuario(int id, string passwordPlainText)
        {
            Usuario usuario = _repository.GetById(id);
            usuario.Contraseña = Encryption.Encrypt(passwordPlainText, _settings.SecretKey);
            return ModificarUsuario(usuario);
        }

        public ServiceResponseEnumerableResult<Usuario> ObtenerUsuarios()
        {
            try
            {
                return new ServiceResponseEnumerableResult<Usuario>(_repository.GetAllActive());
            }
            catch (Exception ex)
            {

                return new ServiceResponseEnumerableResult<Usuario>(false, ex.Message);
            }
        }

       
    }
}
