﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class MercadoPagoCredencialService : IMercadoPagoCredencialService
    {
        private readonly IMercadoPagoCredencialRepository _repository;

        public MercadoPagoCredencialService(IMercadoPagoCredencialRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseResult<MercadoPagoCredencial> AgregarCredencial(MercadoPagoCredencial mercadoPagoCredencial)
        {
            try
            {
                return new ServiceResponseResult<MercadoPagoCredencial>(_repository.Insert(mercadoPagoCredencial));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<MercadoPagoCredencial>(false, ex.Message);
            }
        }

        public ServiceResponseResult<MercadoPagoCredencial> ModificarCredencial(MercadoPagoCredencial mercadoPagoCredencial)
        {
            try
            {
                return new ServiceResponseResult<MercadoPagoCredencial>(_repository.Update(mercadoPagoCredencial));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<MercadoPagoCredencial>(false, ex.Message);
            }
        }

        public ServiceResponseResult<MercadoPagoCredencial> ObtenerCredencial(ProyectosEnum proyecto)
        {
            try
            {
                return new ServiceResponseResult<MercadoPagoCredencial>(_repository.ObtenerCredencialPorProyecto(proyecto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<MercadoPagoCredencial>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<MercadoPagoCredencial> ObtenerCredencial()
        {
            try
            {
                return new ServiceResponseEnumerableResult<MercadoPagoCredencial>(_repository.GetAllActive());
            }
            catch (Exception ex)
            {

                return new ServiceResponseEnumerableResult<MercadoPagoCredencial>(false, ex.Message);
            }
        }
    }
}
