﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class MercadoPagoVentaService : IMercadoPagoVentaService
    {
        private readonly IMercadoPagoVentaRepository _repository;

        public MercadoPagoVentaService(IMercadoPagoVentaRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseResult<MercadoPagoVenta> AgregarMercadoPagoVenta(MercadoPagoVenta mercadoPagoVenta)
        {
            try
            {
                return new ServiceResponseResult<MercadoPagoVenta>(_repository.Insert(mercadoPagoVenta));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<MercadoPagoVenta>(false, ex.Message);
            }
        }


        public ServiceResponseResult<MercadoPagoVenta> ModificarMercadoPagoVenta(MercadoPagoVenta mercadoPagoVenta)
        {
            throw new NotImplementedException();
        }

        public ServiceResponseResult<MercadoPagoVenta> ObtenerVentas(int idVenta)
        {
            try
            {
                return new ServiceResponseResult<MercadoPagoVenta>(_repository.ObtenerVentas(idVenta));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<MercadoPagoVenta>(false, ex.Message);
            }
        }
    }
}
