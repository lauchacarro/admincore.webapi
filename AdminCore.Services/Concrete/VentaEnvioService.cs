﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class VentaEnvioService : IVentaEnvioService
    {
        private readonly IVentaEnvioRepository _repository;

        public VentaEnvioService(IVentaEnvioRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseResult<VentaEnvio> AgregarVentaEnvio(VentaEnvio ventaEnvio)
        {
            try
            {
                return new ServiceResponseResult<VentaEnvio>(_repository.Insert(ventaEnvio));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaEnvio>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaEnvio> DesactivarVentaEnvio(int idVentaEnvio)
        {
            VentaEnvio ventaEnvio = _repository.GetById(idVentaEnvio);
            return ModificarVentaEnvio(ventaEnvio);

        }

        public ServiceResponseResult<VentaEnvio> ModificarVentaEnvio(VentaEnvio ventaEnvio)
        {
            try
            {
                return new ServiceResponseResult<VentaEnvio>(_repository.Update(ventaEnvio));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaEnvio>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaEnvio> ObtenerVentaEnvio(int idVenta)
        {
            try
            {
                return new ServiceResponseResult<VentaEnvio>(_repository.ObtenerEnvioDeVenta(idVenta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaEnvio>(false, ex.Message);
            }
        }

        public ServiceResponseResult<VentaEnvio> ObtenerVentaEnvioPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<VentaEnvio>(_repository.GetById(id));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<VentaEnvio>(false, ex.Message);
            }
        }
    }
}
