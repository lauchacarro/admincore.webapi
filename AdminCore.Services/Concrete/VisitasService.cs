﻿
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class VisitaService : IVisitaService
    {
        readonly IVisitaRepository _visitaRepository;

        public VisitaService(IVisitaRepository visitaRepository)
        {
            _visitaRepository = visitaRepository;
        }

        public void Agregar(ProyectosEnum proyecto, string ip)
        {
            _visitaRepository.Insert(new Business.Dto.Visita()
            {
                Proyecto = proyecto,
                Ip = ip,
                FechaAlta = DateTime.Now,
                Activo = true
            });
        }

        public IEnumerable<Business.Dto.Visita> ObtenerVisitas(ProyectosEnum proyecto)
        {
            if(proyecto == ProyectosEnum.AdminCore)
            {
                return _visitaRepository.GetAllActive();
            }
            else
            {
                return _visitaRepository.GetVisitasByProyect(proyecto);
            }
           
        }
    }
}
