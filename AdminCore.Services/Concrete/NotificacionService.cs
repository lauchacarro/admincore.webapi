﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Hub.SDK;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace AdminCore.Services.Concrete
{
    public class NotificacionService : INotificacionService
    {
        readonly INotificacionRepository _repository;

        public NotificacionService(INotificacionRepository repository, IConfiguration configuration)
        {
            _repository = repository;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public async Task AgregarNotificacion(Notificacion notificacion)
        {
            await Agregar(notificacion);
        }

        public ServiceResponseResult<Notificacion> DesactivarNotificacion(int idNotificacion)
        {
            Notificacion notificacion = _repository.GetById(idNotificacion);
            notificacion.Activo = false;
            return ModificarNotificacion(notificacion);
        }

        public ServiceResponseResult<Notificacion> ModificarNotificacion(Notificacion notificacion)
        {
            try
            {
                return new ServiceResponseResult<Notificacion>(_repository.Update(notificacion));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Notificacion>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Notificacion> ObtenerNotificaciones(ProyectosEnum proyecto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Notificacion>(_repository.ObtenerNotificacionesPorProyecto(proyecto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Notificacion>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Notificacion> ObtenerNotificacionPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<Notificacion>(_repository.GetById(id));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Notificacion>(false, ex.Message);
            }
        }

        private async Task Agregar(Notificacion notificacion)
        {
            AdminCore.Hub.SDK.Configuration.Token = null;
            AdminCore.Hub.SDK.Configuration.UrlHub = Configuration["Urls:AdminCoreHubNotificacion"];
            AdminCore.Hub.SDK.Configuration.UrlAuth = Configuration["Settings:Issuer"] + "/api/auth/login";
            NotificationSDKService notificationSDKService = new NotificationSDKService();
            await notificationSDKService.Create(notificacion);
            
        }
    }
}
