﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.Services.Concrete
{
    public class ImagenService : IImagenService
    {
        IImagenRepository _repository;

        IImgurService _imgurService;

        public ImagenService(IImagenRepository repository, IImgurService imgurService)
        {
            _repository = repository;
            _imgurService = imgurService;
        }

        public ServiceResponseResult<Imagen> AgregarImagen(Imagen imagen)
        {
            try
            {
                return new ServiceResponseResult<Imagen>(_repository.Insert(imagen));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Imagen>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Imagen> AgregarImagen(byte[] imagen)
        {
            var img = _imgurService.SubirImagenAsync(imagen).GetAwaiter().GetResult();
            Imagen _imagen = new Imagen()
            {
                Activo = true,
                FechaAlta = DateTime.Now,
                Link = img.Link,
                Stream = imagen
            };
            return AgregarImagen(_imagen);
        }

        public ServiceResponseResult<Imagen> DesactivarImagen(int idImagen)
        {
            Imagen imagen = _repository.GetById(idImagen);
            imagen.Activo = false;
            return ModificarImagen(imagen);
        }

        public ServiceResponseResult<Imagen> ModificarImagen(Imagen imagen)
        {
            try
            {
                return new ServiceResponseResult<Imagen>(_repository.Update(imagen));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Imagen>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Imagen> ModificarImagen(int id, byte[] imagen)
        {
            var img = _imgurService.SubirImagenAsync(imagen).GetAwaiter().GetResult();
            Imagen _imagen = _repository.GetById(id);

            _imagen.Link = img.Link;
            _imagen.Stream = imagen;

            return ModificarImagen(_imagen);
        }

        public ServiceResponseResult<Imagen> ObtenerImagenPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<Imagen>(_repository.GetById(id));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Imagen>(false, ex.Message);
            }
        }

        public async Task SincronizarAImgur()
        {
            List<Imagen> imagens = _repository.GetAllActive().ToList();
            foreach (Imagen imagen in imagens.Where(x => x.Id > 100))
            {
                try
                {
                    var imgur = _imgurService.SubirImagenAsync(imagen.Stream).GetAwaiter().GetResult();
                    imagen.Link = imgur.Link;
                    _repository.Update(imagen);
                }
                catch (Exception)
                {


                }

            }
        }
    }
}
