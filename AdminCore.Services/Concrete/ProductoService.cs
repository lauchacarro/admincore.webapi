﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class ProductoService : IProductoService
    {
        readonly IProductoRepository _repository;

        readonly IPromoService _promoService;

        public ProductoService(IProductoRepository repository, IPromoService promoService)
        {
            _repository = repository;
            _promoService = promoService;
        }

        public ServiceResponseResult<Producto> AgregarProducto(Producto producto)
        {
            try
            {
                return new ServiceResponseResult<Producto>(_repository.Insert(producto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Producto>(false, ex.Message);
            }
           
        }

        public ServiceResponseResult<Producto> DesactivarProducto(int id)
        {
            List<Promo> promos = (List<Promo>)_promoService.ObtenerPromos(id).Result;
            if (promos.Count > 0)
            {
                return new ServiceResponseResult<Producto>(false, "No se puede eliminar un producto que contenga promos.");
            }
            Producto producto = (Producto)ObtenerProductoPorId(id).Result;
            producto.Activo = false;
            return ModificarProducto(producto);
        }

        public ServiceResponseResult<Producto> ModificarProducto(Producto producto)
        {
            try
            {
                return new ServiceResponseResult<Producto>(_repository.Update(producto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Producto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Producto> ObtenerProductoPorId(int id)
        {

            try
            {
                return new ServiceResponseResult<Producto>(_repository.GetById(id));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Producto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Producto> ObtenerProductos(ProyectosEnum proyecto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Producto>(_repository.ObtenerProductosPorProyecto(proyecto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseEnumerableResult<Producto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Producto> ObtenerProductosPorCategoria(int idCategoria)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Producto>(_repository.ObtenerProductosActivosPorCategoria(idCategoria));
            }
            catch (Exception ex)
            {

                return new ServiceResponseEnumerableResult<Producto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Producto> ObtenerProductosPorSubCategoria(int idSubCategoria)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Producto>(_repository.ObtenerProductosActivosPorSubCategoria(idSubCategoria));
            }
            catch (Exception ex)
            {

                return new ServiceResponseEnumerableResult<Producto>(false, ex.Message);
            }
        }
    }
}
