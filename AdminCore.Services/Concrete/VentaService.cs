﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class VentaService : IVentaService
    {
        private readonly IVentaRepository _repository;

        public VentaService(IVentaRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseResult<Venta> AgregarVenta(Venta venta)
        {
            try
            {
                return new ServiceResponseResult<Venta>(_repository.Insert(venta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Venta>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Venta> DesactivarVenta(int idVenta)
        {
            Venta venta = _repository.GetById(idVenta);
            venta.Activo = false;
            return ModificarVenta(venta);
        }

        public ServiceResponseResult<Venta> ModificarVenta(Venta venta)
        {
            try
            {
                return new ServiceResponseResult<Venta>(_repository.Update(venta));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Venta>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Venta> ObtenerVentaPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<Venta>(_repository.GetById(id));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Venta>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Venta> ObtenerVentas(ProyectosEnum proyecto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Venta>(_repository.ObtenerVentasPorProyecto(proyecto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Venta>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Venta> ObtenerVentas(ProyectosEnum proyecto, VentaEstadosEnum estado)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Venta>(_repository.ObtenerVentasPorProyecto(proyecto, estado));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Venta>(false, ex.Message);
            }
        }
    }
}
