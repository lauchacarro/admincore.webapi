﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class ContactoService : IContactoService
    {
        IContactoRepository _repository;

        public ContactoService(IContactoRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponseResult<Contacto> AgregarContacto(Contacto contacto)
        {
            try
            {
                return new ServiceResponseResult<Contacto>(_repository.Insert(contacto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Contacto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Contacto> DesactivarContacto(int idContacto)
        {
            Contacto contacto = _repository.GetById(idContacto);
            contacto.Activo = false;
            return ModificarContacto(contacto);
        }

        public ServiceResponseResult<Contacto> ModificarContacto(Contacto contacto)
        {
            try
            {
                return new ServiceResponseResult<Contacto>(_repository.Update(contacto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Contacto>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Contacto> ObtenerContactoPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<Contacto>(_repository.GetById(id));
            }
            catch (Exception ex)
            {

                return new ServiceResponseResult<Contacto>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Contacto> ObtenerContactos(ProyectosEnum proyecto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Contacto>(_repository.ObtenerContactosPorProyecto(proyecto));
            }
            catch (Exception ex)
            {

                return new ServiceResponseEnumerableResult<Contacto>(false, ex.Message);
            }
        }
    }
}
