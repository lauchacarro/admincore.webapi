﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Abstract;
using MercadoPago;
using MercadoPago.DataStructures.Preference;
using MercadoPago.Resources;
using System;
using System.Collections.Generic;

namespace AdminCore.Services.Concrete
{
    public class MercadoPagoService : IMercadoPagoService
    {
        IMercadoPagoCredencialService _credencialService;
        IVentaService _ventaService;
        IVentaEnvioService _ventaEnvioService;
        IVentaItemService _ventaItemService;
        IContactoService _contactoService;
        IProductoService _productoService;
        IPromoService _promoService;

        public MercadoPagoService(IMercadoPagoCredencialService credencialService, IVentaService ventaService, IVentaEnvioService ventaEnvioService, IVentaItemService ventaItemService, IContactoService contactoService, IProductoService productoService, IPromoService promoService)
        {
            _credencialService = credencialService;
            _ventaService = ventaService;
            _ventaEnvioService = ventaEnvioService;
            _ventaItemService = ventaItemService;
            _contactoService = contactoService;
            _productoService = productoService;
            _promoService = promoService;
        }
        ProyectosEnum proyecto;

        public Preference CrearPreference(Venta venta)
        {
            proyecto = venta.Proyecto;
            MercadoPagoCredencial credenciales = _credencialService.ObtenerCredencial(venta.Proyecto).Result;
            List<VentaItem> listaItem = _ventaItemService.ObtenerIdsItemsDeVenta(venta.Id).Result;
            VentaEnvio ventaEnvio = _ventaEnvioService.ObtenerVentaEnvio(venta.Id).Result;
            Contacto contacto = new Contacto();

            if (venta.IdContacto.HasValue)
            {
                contacto = _contactoService.ObtenerContactoPorId(venta.IdContacto.Value).Result;
            }

            SDK sdk = SetearCredenciales(credenciales);

            Preference preference = new Preference(sdk)
            {

                Items = ObtenerItems(listaItem),
                Payer = ObtenerPayer(contacto, ventaEnvio),
                NotificationUrl = $"https://admincore.herokuapp.com/api/ventas/Notificacion/" + venta.Id
            };
            preference.Save();
            return preference;

        }

        private SDK SetearCredenciales(MercadoPagoCredencial credenciales)
        {
            SDK sdk = new SDK();

            sdk.ClientId = credenciales.ClientId;
            sdk.ClientSecret = credenciales.ClientSecret;
            return sdk;
        }

        private List<Item> ObtenerItems(List<VentaItem> listaVentaItem)
        {
            List<Item> listItem = new List<Item>();
            listItem.Add(new Item()
            {
                Id = "0",
                Title = "Productos o Servicios " + proyecto.GetDescription(),
                Quantity = 1,
                CurrencyId = MercadoPago.Common.CurrencyId.ARS,
                UnitPrice = 0
            });
            foreach (VentaItem ventaItem in listaVentaItem)
            {
                listItem.Add(VentaItemToItem(ventaItem));
            }
            return listItem;
        }
        private Item VentaItemToItem(VentaItem item)
        {
            Item mpItem = new Item();
            if (item.IdProducto.HasValue)
            {
                Producto producto = _productoService.ObtenerProductoPorId(item.IdProducto.Value).Result;
                mpItem = new Item()
                {
                    Id = item.IdProducto.Value.ToString(),
                    Title = producto.Nombre,
                    Quantity = item.CantidadUnidades,
                    CurrencyId = MercadoPago.Common.CurrencyId.ARS,
                    UnitPrice = producto.Precio,
                    CategoryId = producto.IdCategoria,
                    Description = producto.Descripcion
                };
            }
            if (item.IdPromo.HasValue)
            {
                Promo promo = _promoService.ObtenerPromoPorId(item.IdPromo.Value).Result;
                mpItem = new Item()
                {
                    Id = item.IdPromo.Value.ToString(),
                    Title = promo.Descripcion,
                    Quantity = item.CantidadUnidades,
                    CurrencyId = MercadoPago.Common.CurrencyId.ARS,
                    UnitPrice = promo.Precio

                };
            }
            return mpItem;
        }
        private Payer ObtenerPayer(Contacto contacto, VentaEnvio ventaEnvio)
        {
            return new Payer()
            {
                Name = contacto.Nombre,
                Surname = contacto.Apellido,
                Email = contacto.Mail,
                Phone = new Phone()
                {
                    AreaCode = "",
                    Number = contacto.Telefono
                },
                //Identification = new Identification()
                //{
                //    Type = "DNI",
                //    Number = "12345678"
                //},

                Address = new Address()
                {
                    StreetName = ventaEnvio != null ? ventaEnvio.Direccion : null
                }
            };
        }

        public Payment ObtenerPayment(int idVenta, long idMercadoPagoPayment)
        {
            Venta venta = _ventaService.ObtenerVentaPorId(idVenta).Result;
            MercadoPagoCredencial credenciales = _credencialService.ObtenerCredencial(venta.Proyecto).Result;
            try
            {

                SDK sdk = SetearCredenciales(credenciales);
                Payment payment = Payment.FindById(idMercadoPagoPayment, sdk);
                if (payment.Errors == null)
                {
                    return payment;
                }

                return null;
            }
            catch (Exception ex)
            {

                return null;
            }



        }
    }
}
