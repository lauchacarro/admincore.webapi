﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Services.Concrete
{
    public class CategoriaService : ICategoriaService
    {
        readonly ICategoriaRepository _repository;
        readonly IProductoService _productoService;
        readonly ISubCategoriaService _subCategoriaService;
        public CategoriaService(ICategoriaRepository repository, IProductoService productoService, ISubCategoriaService subCategoriaService)
        {
            _repository = repository;
            _productoService = productoService;
            _subCategoriaService = subCategoriaService;
        }

        public ServiceResponseResult<Categoria> AgregarCategoria(Categoria categoria)
        {
            try
            {
                return new ServiceResponseResult<Categoria>(_repository.Insert(categoria) );
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Categoria>(false, ex.Message);
            }

        }

        public ServiceResponseResult<Categoria> DesactivarCategoria(int idCategoria)
        {

            List<Producto> productos = _productoService.ObtenerProductosPorCategoria(idCategoria).Result;
            if (productos.Count > 0)
            {
                return new ServiceResponseResult<Categoria>(false, "No se puede eliminar una Categoria que contengan productos activos." );
            }
            IEnumerable<SubCategoria> listaSubCategorias = _subCategoriaService.ObtenerSubCategorias(idCategoria).Result;

            foreach (SubCategoria subCategoria in listaSubCategorias)
            {
                _subCategoriaService.DesactivarSubCategoria(subCategoria.Id);
            }

            Categoria categoria = _repository.GetById(idCategoria);
            categoria.Activo = false;
            return ModificarCategoria(categoria);

        }

        public ServiceResponseResult<Categoria> ModificarCategoria(Categoria categoria)
        {
            try
            {
                return new ServiceResponseResult<Categoria>(_repository.Update(categoria) );
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Categoria>(false, ex.Message);
            }

        }

        public ServiceResponseResult<Categoria> ObtenerCategoriaPorId(int id)
        {

            try
            {
                return new ServiceResponseResult<Categoria>(_repository.GetById(id) );
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Categoria>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Categoria> ObtenerCategorias(ProyectosEnum proyecto)
        {

            try
            {
                return new ServiceResponseEnumerableResult<Categoria>(_repository.ObtenerCategoriasPorProyecto(proyecto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Categoria>(false, ex.Message);
            }

        }
    }
}
