﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminCore.Services.Concrete
{
    public class ProyectoService : IProyectoService
    {
        readonly IProyectoRepository _repository;

        public ProyectoService(IProyectoRepository repository)
        {
            _repository = repository;
        }

        public void AgregarProyecto(Proyecto proyecto)
        {
            proyecto.Activo = true;
            _repository.Insert(proyecto);
        }

        public void DesactivarProyecto(Proyecto proyecto)
        {
            proyecto.Activo = false;
            ModificarProyecto(proyecto);
        }

        public void ModificarProyecto(Proyecto proyecto)
        {
            _repository.Update(proyecto);
        }

        public Proyecto ObtenerProyectoPorId(int id)
        {
            return _repository.GetById(id);
        }

        public List<Proyecto> ObtenerProyectos()
        {
            return _repository.GetAllActive().ToList();
        }
    }
}
