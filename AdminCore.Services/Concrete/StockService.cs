﻿using AdminCore.Business.Dto;
using AdminCore.Repositories.Abstract;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Concrete
{
    public class StockService : IStockService
    {

        readonly IStockRepository _repository;

        readonly IProductoService _productoService;

        public StockService(IStockRepository repository, IProductoService productoService)
        {
            _repository = repository;
            _productoService = productoService;
        }

        public ServiceResponseResult<Stock> AgregarStock(Stock stock)
        {
            try
            {
                Stock stockInsertado = _repository.Insert(stock);
                ActualizarStockDeProducto(stockInsertado.IdProducto);
                return new ServiceResponseResult<Stock>(stockInsertado);
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Stock>(false, ex.Message);
            }

        }

        public ServiceResponseResult<Stock> DesactivarStock(int id)
        {
            Stock stock = _repository.GetById(id);
            stock.Activo = false;
            return ModificarStock(stock);
        }

        public ServiceResponseResult<Stock> ModificarStock(Stock stock)
        {
            try
            {
                Stock stockModificado = _repository.Update(stock);
                ActualizarStockDeProducto(stockModificado.IdProducto);
                return new ServiceResponseResult<Stock>(stockModificado);
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Stock>(false, ex.Message);
            }
        }

        public ServiceResponseResult<Stock> ObtenerStockPorId(int id)
        {
            try
            {
                return new ServiceResponseResult<Stock>( _repository.GetById(id));
            }
            catch (Exception ex)
            {
                return new ServiceResponseResult<Stock>(false, ex.Message);
            }
        }

        public ServiceResponseEnumerableResult<Stock> ObtenerStocks(int idProducto)
        {
            try
            {
                return new ServiceResponseEnumerableResult<Stock>(_repository.ObtenerStockPorProducto(idProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponseEnumerableResult<Stock>(false, ex.Message);
            }
        }

        public ServiceResponse ObtenerStockTotalDeProcucto(int idProducto)
        {
            try
            {
                return new ServiceResponse(_repository.ObtenerStockTotalDeProcucto(idProducto));
            }
            catch (Exception ex)
            {
                return new ServiceResponse(false, ex.Message);
            }
        }

        private void ActualizarStockDeProducto(int idProducto)
        {
            int cantStockActual = _repository.ObtenerStockTotalDeProcucto(idProducto);
            Producto producto = _productoService.ObtenerProductoPorId(idProducto).Result;
            producto.Stock = cantStockActual;
            _productoService.ModificarProducto(producto);
        }
    }
}
