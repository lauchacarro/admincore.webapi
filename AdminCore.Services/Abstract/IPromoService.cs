﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IPromoService 
    {

        ServiceResponseResult<Promo> AgregarPromo(Promo promo);

        
        ServiceResponseEnumerableResult<Promo> ObtenerPromos(ProyectosEnum idProyecto);

        ServiceResponseEnumerableResult<Promo> ObtenerPromos(int idProducto);

        ServiceResponseEnumerableResult<Promo> ObtenerPromosPorTipo(PromoTipoEnum promoTipo);

        ServiceResponseEnumerableResult<Promo> ObtenerPromosValidasHoy();

        ServiceResponseResult<Promo> ObtenerPromoPorId(int idPromo);

        ServiceResponseResult<Promo> ModificarPromo(Promo promo);

        ServiceResponseResult<Promo> DesactivarPromo(int idPromo);

        
    }
}
