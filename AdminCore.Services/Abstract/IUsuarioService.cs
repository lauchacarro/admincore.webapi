﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IUsuarioService
    {

        ServiceResponseResult<Usuario> ObtenerPorId(int id);

        bool EsValido(ref Usuario user, out string msgError);

        ServiceResponseResult<Usuario> AgregarUsuario(Usuario usuario);
        ServiceResponseResult<Usuario> ModificarUsuario(Usuario usuario);
        ServiceResponseResult<Usuario> DesactivarUsuario(int id);

        ServiceResponseResult<Usuario> CambiarContraseñaUsuario(int id, string passwordPlainText);

        ServiceResponseEnumerableResult<Usuario> ObtenerUsuarios();


    }
}
