﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IVentaProductoService
    {
        ServiceResponseResult<VentaProducto> QuitarProductoDeVenta(int idProducto, int idVenta);

        ServiceResponseEnumerableResult<VentaProducto> ObtenerIdsProductosDeVenta(int idVenta);

        ServiceResponseEnumerableResult<VentaProducto> ObtenerIdsVentas(int idProducto);

        ServiceResponseEnumerableResult<VentaProducto> AgregarProductosAVenta(IEnumerable<Producto> productos, int idVenta);

        ServiceResponseResult<VentaProducto> AgregarVentaProducto(VentaProducto ventaProducto);



        ServiceResponseResult<VentaProducto> ModificarVentaProducto(VentaProducto ventaProducto);
    }
}
