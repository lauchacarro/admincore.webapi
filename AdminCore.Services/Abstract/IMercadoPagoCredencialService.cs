﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IMercadoPagoCredencialService
    {
        ServiceResponseResult<MercadoPagoCredencial> AgregarCredencial(MercadoPagoCredencial mercadoPagoCredencial);

        ServiceResponseResult<MercadoPagoCredencial> ObtenerCredencial(ProyectosEnum proyecto);

        ServiceResponseEnumerableResult<MercadoPagoCredencial> ObtenerCredencial();

        ServiceResponseResult<MercadoPagoCredencial> ModificarCredencial(MercadoPagoCredencial mercadoPagoCredencial);

    }
}
