﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IStockService 
    {
        ServiceResponseResult<Stock> AgregarStock(Stock stock);

        ServiceResponseEnumerableResult<Stock> ObtenerStocks(int idProducto);

        ServiceResponseResult<Stock> ObtenerStockPorId(int id);

        ServiceResponseResult<Stock> ModificarStock(Stock stock);

        ServiceResponseResult<Stock> DesactivarStock(int id);

        ServiceResponse ObtenerStockTotalDeProcucto(int idProducto);
    }
}
