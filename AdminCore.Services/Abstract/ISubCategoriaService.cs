﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface ISubCategoriaService
    {
        ServiceResponseResult<SubCategoria> AgregarSubCategoria(SubCategoria subCategoria);

        ServiceResponseEnumerableResult<SubCategoria> ObtenerSubCategorias(int idCategoria);

        ServiceResponseResult<SubCategoria> ObtenerSubCategoriaPorId(int id);

        ServiceResponseResult<SubCategoria> ModificarSubCategoria(SubCategoria subCategoria);

        ServiceResponseResult<SubCategoria> DesactivarSubCategoria(int id);
    }
}
