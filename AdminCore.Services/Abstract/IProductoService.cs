﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IProductoService
    {
        ServiceResponseResult<Producto> AgregarProducto(Producto producto);

        ServiceResponseEnumerableResult<Producto> ObtenerProductos(ProyectosEnum proyecto);

        ServiceResponseResult<Producto> ObtenerProductoPorId(int id);

        ServiceResponseEnumerableResult<Producto> ObtenerProductosPorCategoria(int idCategoria);

        ServiceResponseEnumerableResult<Producto> ObtenerProductosPorSubCategoria(int idSubCategoria);

        ServiceResponseResult<Producto> ModificarProducto(Producto producto);

        ServiceResponseResult<Producto> DesactivarProducto(int id);
    }
}
