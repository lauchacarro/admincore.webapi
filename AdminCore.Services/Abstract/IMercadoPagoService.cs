﻿using System;
using System.Collections.Generic;
using System.Text;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using MercadoPago;
using MercadoPago.DataStructures;
using MercadoPago.Resources;

namespace AdminCore.Services.Abstract
{
    public interface IMercadoPagoService
    {
        Preference CrearPreference(Venta venta);

        Payment ObtenerPayment(int idVenta, long idMercadoPagoPayment);

    }
}
