﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IContactoService
    {
        ServiceResponseResult<Contacto> AgregarContacto(Contacto contacto);

        ServiceResponseEnumerableResult<Contacto> ObtenerContactos(ProyectosEnum proyecto);

        ServiceResponseResult<Contacto> ObtenerContactoPorId(int id);

        ServiceResponseResult<Contacto> ModificarContacto(Contacto contacto);

        ServiceResponseResult<Contacto> DesactivarContacto(int idContacto);
    }
}
