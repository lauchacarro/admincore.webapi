﻿using AdminCore.Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IProyectoService
    {
        void AgregarProyecto(Proyecto proyecto);

        List<Proyecto> ObtenerProyectos();

        Proyecto ObtenerProyectoPorId(int id);

        void ModificarProyecto(Proyecto proyecto);

        void DesactivarProyecto(Proyecto proyecto);
    }
}
