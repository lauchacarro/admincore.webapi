﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface ICategoriaService
    {
        ServiceResponseResult<Categoria> AgregarCategoria(Categoria categoria);

        ServiceResponseEnumerableResult<Categoria> ObtenerCategorias(ProyectosEnum proyecto);

        ServiceResponseResult<Categoria> ObtenerCategoriaPorId(int id);

        ServiceResponseResult<Categoria> ModificarCategoria(Categoria categoria);

        ServiceResponseResult<Categoria> DesactivarCategoria(int idCategoria);
    }
}
