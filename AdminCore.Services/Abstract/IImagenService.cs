﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Services.Abstract
{
    public interface IImagenService
    {
        ServiceResponseResult<Imagen> AgregarImagen(byte[] imagen);
        ServiceResponseResult<Imagen> AgregarImagen(Imagen imagen);

        ServiceResponseResult<Imagen> ObtenerImagenPorId(int id);

        ServiceResponseResult<Imagen> ModificarImagen(int id, byte[] imagen);
        ServiceResponseResult<Imagen> ModificarImagen(Imagen imagen);

        ServiceResponseResult<Imagen> DesactivarImagen(int idImagen);

        Task SincronizarAImgur();
    }
}
