﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IMercadoPagoVentaService
    {

        ServiceResponseResult<MercadoPagoVenta> ObtenerVentas(int idMercadoPagoCollector);


        ServiceResponseResult<MercadoPagoVenta> AgregarMercadoPagoVenta(MercadoPagoVenta mercadoPagoVenta);


        ServiceResponseResult<MercadoPagoVenta> ModificarMercadoPagoVenta(MercadoPagoVenta mercadoPagoVenta);
    }
}
