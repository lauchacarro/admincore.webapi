﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Services.Abstract
{
    public interface INotificacionService
    {
        Task AgregarNotificacion(Notificacion notificacion);

        ServiceResponseEnumerableResult<Notificacion> ObtenerNotificaciones(ProyectosEnum proyecto);

        ServiceResponseResult<Notificacion> ObtenerNotificacionPorId(int id);

        ServiceResponseResult<Notificacion> ModificarNotificacion(Notificacion notificacion);

        ServiceResponseResult<Notificacion> DesactivarNotificacion(int idNotificacion);
    }
}
