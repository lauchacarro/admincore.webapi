﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;

namespace AdminCore.Services.Abstract
{
    public interface IPromoTemporalService
    {
        ServiceResponseResult<PromoTemporal> AgregarPromoTemporal(PromoTemporal promoTemporal);

        ServiceResponseResult<PromoTemporal> ObtenerPromoTemporalPorPromo(int idPromo);

        ServiceResponseResult<PromoTemporal> ObtenerPromoTemporalPorId(int idPromoTemporal);

        ServiceResponseResult<PromoTemporal> ModificarPromoTemporal(PromoTemporal promoTemporal);

        ServiceResponseEnumerableResult<PromoTemporal> ObtenerPromosValidasEnRangoDeFechas(DateTime fechaDesde, DateTime fechaHasta);
    }
}
