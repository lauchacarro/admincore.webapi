﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IPromoProductoService
    {
        ServiceResponseResult<PromoProducto> QuitarProductoDePromo(int idProducto, int idPromo);

        ServiceResponseEnumerableResult<PromoProducto> ObtenerIdsProductosDePromo(int idPromo);

        ServiceResponseEnumerableResult<PromoProducto> ObtenerIdsPromos(int idProducto);

        ServiceResponseEnumerableResult<PromoProducto> AgregarProductosAPromo(IEnumerable<Producto> productos, int idPromo);

        ServiceResponseResult<PromoProducto> AgregarPromoProducto(PromoProducto promoProducto);



        ServiceResponseResult<PromoProducto> ModificarPromoProducto(PromoProducto promoProducto);
    }
}
