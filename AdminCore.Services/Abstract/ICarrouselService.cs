﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface ICarrouselService
    {
        ServiceResponseResult<Carrousel> AgregarCarrousel(Carrousel carrousel);

        ServiceResponseEnumerableResult<Carrousel> ObtenerCarrouseles(ProyectosEnum proyecto);

        ServiceResponseResult<Carrousel> ObtenerCarrouselPorId(int id);

        ServiceResponseResult<Carrousel> ModificarCarrousel(Carrousel carrousel);

        ServiceResponseResult<Carrousel> DesactivarCarrousel(int idCarrousel);
    }
}
