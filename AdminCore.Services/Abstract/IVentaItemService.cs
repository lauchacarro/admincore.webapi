﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IVentaItemService
    {
        ServiceResponseResult<VentaItem> QuitarProductoDeVenta(int idProducto, int idVenta);

        ServiceResponseResult<VentaItem> QuitarPromoDeVenta(int idPromo, int idVenta);

        ServiceResponseResult<VentaItem> QuitarVentaItem(int id);

        ServiceResponseEnumerableResult<VentaItem> ObtenerIdsItemsDeVenta(int idVenta);

        ServiceResponseEnumerableResult<VentaItem> ObtenerIdsVentasDeProducto(int idProducto);

        ServiceResponseEnumerableResult<VentaItem> ObtenerIdsVentasDePromo(int idPromo);

        ServiceResponseEnumerableResult<VentaItem> AgregarProductosAVenta(IEnumerable<Producto> productos, int idVenta);

        ServiceResponseEnumerableResult<VentaItem> AgregarPromosAVenta(IEnumerable<Promo> promos, int idVenta);

        ServiceResponseResult<VentaItem> AgregarVentaItem(VentaItem ventaItem);
        ServiceResponseEnumerableResult<VentaItem> AgregarVentaItems(IEnumerable<VentaItem> ventaItems);
    }
}
