﻿
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IVisitaService
    {
        void Agregar(ProyectosEnum proyecto, string ip);

        IEnumerable<Business.Dto.Visita> ObtenerVisitas(ProyectosEnum proyecto);
    }
}
