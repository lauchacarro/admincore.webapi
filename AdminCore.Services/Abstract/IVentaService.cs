﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IVentaService
    {
        ServiceResponseResult<Venta> AgregarVenta(Venta venta);

        ServiceResponseEnumerableResult<Venta> ObtenerVentas(ProyectosEnum proyecto);

        ServiceResponseEnumerableResult<Venta> ObtenerVentas(ProyectosEnum proyecto, VentaEstadosEnum estados);

        ServiceResponseResult<Venta> ObtenerVentaPorId(int id);

        ServiceResponseResult<Venta> ModificarVenta(Venta venta);

        ServiceResponseResult<Venta> DesactivarVenta(int idVenta);
    }
}
