﻿using AdminCore.Business.Dto;
using AdminCore.Services.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Abstract
{
    public interface IVentaEnvioService
    {
        ServiceResponseResult<VentaEnvio> AgregarVentaEnvio(VentaEnvio ventaEnvio);

        ServiceResponseResult<VentaEnvio> ObtenerVentaEnvio(int idVenta);

        ServiceResponseResult<VentaEnvio> ObtenerVentaEnvioPorId(int id);

        ServiceResponseResult<VentaEnvio> ModificarVentaEnvio(VentaEnvio ventaEnvio);

        ServiceResponseResult<VentaEnvio> DesactivarVentaEnvio(int idVentaEnvio);
    }
}
