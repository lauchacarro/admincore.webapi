﻿using Imgur.API.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminCore.Services.Abstract
{
    public interface IImgurService
    {
        IImage ObtenerImagen(string idImageImgur);
        Task<IImage> ObtenerImagenAsync(string idImageImgur);

        IImage SubirImagen(byte[] image);
        Task<IImage> SubirImagenAsync(byte[] image);
    }
}
