﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Services.Configuration
{
    public class Settings
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string SecretKey { get; set; }

        public string ImgurClientId { get; set; }

        public string ImgurClientStecret { get; set; }
    }
}
