﻿/*
Plantilla de script posterior a la implementación							
--------------------------------------------------------------------------------------
 Este archivo contiene instrucciones de SQL que se anexarán al script de compilación.		
 Use la sintaxis de SQLCMD para incluir un archivo en el script posterior a la implementación.			
 Ejemplo:      :r .\miArchivo.sql								
 Use la sintaxis de SQLCMD para hacer referencia a una variable en el script posterior a la implementación.		
 Ejemplo:      :setvar TableName miTabla							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF NOT EXISTS(SELECT TOP 1 1 FROM ProyectoTipo)
BEGIN
INSERT INTO ProyectoTipo (Descripcion,Activo) VALUES('Sitio Web', 1)
END



IF NOT EXISTS(SELECT TOP 1 1 FROM ProductoTipo)
BEGIN
INSERT INTO ProductoTipo (Descripcion,Activo) VALUES('Normal', 1)
INSERT INTO ProductoTipo (Descripcion,Activo) VALUES('PorPedido', 1)

END

IF NOT EXISTS(SELECT TOP 1 1 FROM PromoTipo)
BEGIN
INSERT INTO PromoTipo (Descripcion,Activo) VALUES('Permanente', 1)
INSERT INTO PromoTipo (Descripcion,Activo) VALUES('Temporal', 1)

END


IF NOT EXISTS(SELECT TOP 1 1 FROM StockTipoModificacion)
BEGIN
INSERT INTO StockTipoModificacion (Descripcion,Activo) VALUES('Compra', 1)
INSERT INTO StockTipoModificacion (Descripcion,Activo) VALUES('Venta', 1)
INSERT INTO StockTipoModificacion (Descripcion,Activo) VALUES('Sobra', 1)
INSERT INTO StockTipoModificacion (Descripcion,Activo) VALUES('falta', 1)
END

IF NOT EXISTS(SELECT TOP 1 1 FROM UsuarioTipo)
BEGIN
INSERT INTO UsuarioTipo (Descripcion,Activo) VALUES('Omnipotente', 1)
INSERT INTO UsuarioTipo (Descripcion,Activo) VALUES('Omnisciente', 1)
END


IF NOT EXISTS(SELECT TOP 1 1 FROM Proyectos)
BEGIN
INSERT INTO Proyectos (Nombre,IdProyectoTipo, GitUrl,FechaAlta, Activo) VALUES('AdminCore', 1, null,GETDATE(), 1)

END

--http://puromarketing-germanvelasquez.blogspot.com/2011/08/tipos-de-venta_04.html
IF NOT EXISTS(SELECT TOP 1 1 FROM VentaTipo)
BEGIN
INSERT INTO VentaTipo (Descripcion,Activo) VALUES('Personal', 1)
INSERT INTO VentaTipo (Descripcion,Activo) VALUES('Teléfonica', 1)
INSERT INTO VentaTipo (Descripcion,Activo) VALUES('Online', 1)
INSERT INTO VentaTipo (Descripcion,Activo) VALUES('Correo', 1)
INSERT INTO VentaTipo (Descripcion,Activo) VALUES('Máquinas Automáticas', 1)
END


IF NOT EXISTS(SELECT TOP 1 1 FROM VentaEstados)
BEGIN
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Reservada', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Creada', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Pendiente Pagar', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Pagada', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Pendiente Entrega', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Finalizada', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Devuelta', 1)
INSERT INTO VentaEstados (Descripcion,Activo) VALUES('Cancelada', 1)
END



IF NOT EXISTS(SELECT TOP 1 1 FROM Usuarios)
BEGIN
INSERT INTO Usuarios (Alias, Contraseña, IdUsuarioTipo, IdProyecto, FechaAlta, FechaModificacion, Activo ) 
VALUES( 'superadmin', 'uoJZva0hfStfm9ILi5L0UBV1E1/lc4p7aTnAPFRsOXM=', 1, 1, GETDATE(), null, 1) --Admincore1

INSERT INTO Usuarios (Alias, Contraseña, IdUsuarioTipo, IdProyecto, FechaAlta, FechaModificacion, Activo ) 
VALUES( 'admin', 'uoJZva0hfStfm9ILi5L0UMdV7qPy6vrPmU8eaqnSFDU=', 2, 1, GETDATE(), null, 1) --Admincore2
END














--UNIQUE


IF EXISTS(SELECT 1 FROM sys.indexes WHERE [Name] = 'UC_PromoProductos')
DROP INDEX UC_PromoProductos ON PromoProductos;  
GO  

CREATE UNIQUE INDEX [UC_PromoProductos] ON PromoProductos(IdPromo, IdProducto) where Activo = 1


IF EXISTS(SELECT 1 FROM sys.indexes WHERE [Name] = 'UC_VentaProductos')
DROP INDEX UC_VentaProductos ON VentaProductos;  
GO 
CREATE UNIQUE INDEX  [UC_VentaProductos] ON VentaProductos(IdVenta, IdProducto) where Activo = 1

IF EXISTS(SELECT 1 FROM sys.indexes WHERE [Name] = 'UC_VentaItems_Productos')
DROP INDEX UC_VentaItems_Productos ON VentaItems;  
GO 
CREATE UNIQUE INDEX  [UC_VentaItems_Productos] ON VentaItems(IdVenta, IdProducto) where IdPromo IS NULL AND Activo = 1

IF EXISTS(SELECT 1 FROM sys.indexes WHERE [Name] = 'UC_VentaItems_Promos')
DROP INDEX UC_VentaItems_Promos ON VentaItems;  
GO 
CREATE UNIQUE INDEX  [UC_VentaItems_Promos] ON VentaItems(IdVenta, IdPromo) where IdProducto IS NULL AND Activo = 1



--ROLES

IF NOT EXISTS(select 1 from sys.database_principals where name = 'GoodFellasRole')
	CREATE ROLE [GoodFellasRole]

GRANT SELECT ON [Carrousel] TO GoodFellasRole
GRANT SELECT ON [Productos] TO GoodFellasRole
GRANT SELECT ON [ProductoCategorias] TO GoodFellasRole

GRANT SELECT ON [ProductoTipo] TO GoodFellasRole

GRANT SELECT ON [PromoProductos] TO GoodFellasRole
GRANT SELECT ON [Promos] TO GoodFellasRole
GRANT SELECT ON [PromoTipo] TO GoodFellasRole
GRANT SELECT ON [PromoTemporal] TO GoodFellasRole
GRANT SELECT ON [Stocks] TO GoodFellasRole
GRANT SELECT ON [Imagenes] TO GoodFellasRole

