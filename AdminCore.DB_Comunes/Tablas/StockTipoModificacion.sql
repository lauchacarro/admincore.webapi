﻿CREATE TABLE [dbo].[StockTipoModificacion]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Descripcion] Varchar(250) NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_StockTipoModificacion] Primary key (Id)
)
