﻿CREATE TABLE [dbo].[NotificacionUsuarios]
(
	[Id] INT NOT NULL Identity (1,1),
	[IdNotificacion] int NOT NULL,
	[IdUsuario] int NOT NULL,
	[Leido] bit NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_NotificacionUsuarios] Primary Key (Id ASC),
	Constraint [FK_NotificacionUsuarios_Notificaciones] Foreign key (IdNotificacion) References Notificaciones(Id),
	Constraint [FK_NotificacionUsuarios_Usuarios] Foreign key (IdUsuario) References Usuarios(Id)
)
