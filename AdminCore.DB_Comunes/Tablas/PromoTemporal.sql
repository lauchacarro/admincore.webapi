﻿CREATE TABLE [dbo].[PromoTemporal]
(
	[Id] INT NOT NULL Identity(1, 1),
	[IdPromo] INT NOT NULL,
	[FechaInicio] Datetime NOT NULL,
	[FechaFin] Datetime NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] Bit NOT NULL,
	Constraint [PK_PromoTemporal] Primary Key (Id),
	Constraint [FK_PromoTemporal_Promo] Foreign key (IdPromo) References Promos(Id)
)
