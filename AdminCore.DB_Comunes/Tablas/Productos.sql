﻿CREATE TABLE [dbo].[Productos]
(
	[Id] INT NOT NULL Identity (1,1), 
    [IdProyecto] INT NOT NULL, 
    [Nombre] VARCHAR(100) NOT NULL,
	[Descripcion] VARCHAR(1000) NULL,
	[IdProductoTipo] INT NOT NULL,
	[Precio] DECIMAL (18, 2) NOT NULL,
	[Stock] INT NOT NULL, 
    [IdCategoria] INT NOT NULL, 
    [IdSubCategoria] INT NULL, 
	[IdImagen] INT NULL, 
    [FechaAlta] DATETIME NOT NULL,
	[FechaModificacion] DATETIME NULL, 
    [Activo] BIT NOT NULL,
	Constraint [PK_Productos] PRIMARY KEY (Id),
	Constraint [FK_Productos_Proyectos] Foreign key (IdProyecto) References Proyectos(Id),
	Constraint [FK_Productos_ProductoTipo] Foreign key (IdProductoTipo) References ProductoTipo(Id),
	Constraint [FK_Productos_Categorias] Foreign key (IdCategoria) References ProductoCategorias(Id),
	Constraint [FK_Productos_SubCategorias] Foreign key (IdSubCategoria) References ProductoSubCategorias(Id),
	Constraint [FK_Productos_Imagenes] Foreign key (IdImagen) References Imagenes(Id)


)
