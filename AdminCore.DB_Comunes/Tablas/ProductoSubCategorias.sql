﻿CREATE TABLE [dbo].[ProductoSubCategorias]
(
	[Id] INT NOT NULL Identity (1,1),
	[IdCategoria] INT NOT NULL ,
	[Nombre] varchar(250)  NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit  NOT NULL,
	Constraint [PK_ProductoSubCategorias] Primary Key (Id ASC),
	Constraint [FK_ProductoSubCategorias_ProductoCategorias] Foreign key (IdCategoria) References ProductoCategorias(Id),
)
