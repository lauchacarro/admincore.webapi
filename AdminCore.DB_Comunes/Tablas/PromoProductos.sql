﻿CREATE TABLE [dbo].[PromoProductos]
(
	[Id] INT NOT NULL Identity(1, 1),
	[IdPromo] INT NOT NULL,
	[IdProducto] INT NOT NULL,
	[CantidadUnidades] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] BIT NOT NULL,
	CONSTRAINT [PK_PromoProductos] Primary Key (Id),
	CONSTRAINT [FK_PromoProductos_Promo] Foreign Key (IdPromo) References Promos(Id),
	CONSTRAINT [FK_PromoProductos_Productos] Foreign Key (IdProducto) References Productos(Id)
)

    
