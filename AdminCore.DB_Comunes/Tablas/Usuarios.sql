﻿CREATE TABLE [dbo].[Usuarios]
(
	[Id] INT NOT NULL Identity (1,1), 
    [Alias] VARCHAR(50) NOT NULL, 
    [Contraseña] VARCHAR(MAX) NOT NULL, 
    [IdUsuarioTipo] INT NOT NULL, 
    [IdProyecto] INT NOT NULL, 
    [FechaAlta] DATETIME NOT NULL, 
    [FechaModificacion] DATETIME NULL, 
    [Activo] BIT NOT NULL,
	Constraint [PK_Usuarios] Primary Key (Id ASC),
	Constraint [FK_Usuarios_UsuarioTipo] Foreign Key (IdUsuarioTipo) References UsuarioTipo(Id),
	Constraint [FK_Usuarios_IdProyecto] Foreign Key (IdProyecto) References Proyectos(Id),
	CONSTRAINT [Unique_Usuarios_Alias] UNIQUE(Alias)   
)
