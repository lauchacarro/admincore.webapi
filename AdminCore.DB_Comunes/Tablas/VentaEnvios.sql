﻿CREATE TABLE [dbo].[VentaEnvios]
(
	[Id] INT NOT NULL IDENTITY(1,1),
	[IdVenta] INT NOT NULL,
	[Direccion] varchar(250) NOT NULL, 
	[LinkGoogleMaps] varchar(1000) NULL,
	[FechaAlta] Datetime NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_VentaEnvios] Primary Key (Id),
	Constraint [FK_VentaEnvios_Venta] Foreign key (IdVenta) References Ventas(Id),

)
