﻿CREATE TABLE [dbo].[Notificaciones]
(
	[Id] INT NOT NULL Identity (1,1),
	[Titulo] varchar(250) NOT NULL,
	[Descripcion] varchar(250) NOT NULL,
	[Tipo] int NOT NULL,
	[IdProyecto] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_Notificaciones] Primary Key (Id ASC),
	Constraint [FK_Notificaciones_Proyectos] Foreign key (IdProyecto) References Proyectos(Id)
)
