﻿CREATE TABLE [dbo].[Ventas]
(
	[Id] INT NOT NULL Identity (1,1),
	[Monto] DECIMAL(18,2) NOT NULL,
	[IdContacto] INT  NULL,
	[IdVentaTipo] INT NOT NULL,
	[IdVentaEstado] INT NOT NULL,
	[IdProyecto] INT NOT NULL,
	[FechaCambioEstado] DATETIME NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_Ventas] Primary Key (Id ASC),
	Constraint [FK_Ventas_Proyectos] Foreign key (IdProyecto) References Proyectos(Id),
	Constraint [FK_Ventas_VentaTipo] Foreign key (IdVentaTipo) References VentaTipo(Id),
	Constraint [FK_Ventas_VentaEstados] Foreign key (IdVentaEstado) References VentaEstados(Id),
	Constraint [FK_Ventas_Contacto] Foreign key (IdContacto) References Contactos(Id)
)