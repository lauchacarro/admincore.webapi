﻿CREATE TABLE [dbo].[Stocks]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[IdProducto] INT NOT NULL,
	[CantidadModificacion] INT NOT NULL,
	[IdStockTipoModificacion] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] Bit NOT NULL,
	Constraint [PK_Stocks] Primary Key (Id),
	Constraint [FK_Stocks_StockTipoModificacion] Foreign key (IdStockTipoModificacion) References StockTipoModificacion(Id),
)
