﻿CREATE TABLE [dbo].[Contactos]
(
	[Id] INT NOT NULL IDENTITY(1,1),
	[Nombre] varchar(250) NOT NULL,
	[Apellido] varchar(250) NULL,
	[Mail] varchar(500) NOT NULL,
	[Telefono] varchar(250) NULL,
	[IdProyecto] INT NOT NULL,
	[FechaAlta] datetime NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_Contactos] Primary Key (Id),
	Constraint [FK_Contactos_Proyectos] Foreign key (IdProyecto) References Proyectos(Id)
)
