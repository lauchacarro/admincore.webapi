﻿CREATE TABLE [dbo].[Visitas]
(
	[Id] INT NOT NULL Identity (1,1), 
    [IdProyecto] INT NOT NULL, 
	[Ip] Varchar(20) NOT NULL, 
	[FechaAlta] DATETIME NOT NULL,
    [Activo] BIT NOT NULL,
	Constraint [PK_Visitas] PRIMARY KEY (Id),
	Constraint [FK_Visitas_Proyectos] Foreign key (IdProyecto) References Proyectos(Id),
)
