﻿CREATE TABLE [dbo].[Imagenes]
(
	[Id] INT NOT NULL Identity (1,1), 
	[Link] VARCHAR(MAX)  NULL,
	[Stream] VARBINARY(MAX)  NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_Imagenes] PRIMARY KEY (Id),

)