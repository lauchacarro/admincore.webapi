﻿CREATE TABLE [dbo].[PromoTipo]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Descripcion] VARCHAR(250) NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_PromoTipo] Primary Key (Id asc)
)
