﻿CREATE TABLE [dbo].[MercadoPagoCredenciales]
(
	[Id] INT NOT NULL IDENTITY (1,1),
	[IdProyecto] INT NOT NULL,
	[ClientId] VARCHAR(3000) NOT NULL,
	[ClientSecret] VARCHAR(3000) NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] BIT NOT NULL,
	Constraint [PK_MercadoPagoCredenciales] Primary Key (Id ASC),
	Constraint [FK_MercadoPagoCredenciales_Proyectos] Foreign key (IdProyecto) References Proyectos(Id)
)
