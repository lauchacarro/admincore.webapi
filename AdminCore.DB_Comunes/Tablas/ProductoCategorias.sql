﻿CREATE TABLE [dbo].[ProductoCategorias]
(
	[Id] INT NOT NULL Identity (1,1),
	[Nombre] varchar(250) NOT NULL,
	[IdProyecto] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_ProductoCategorias] Primary Key (Id ASC),
	Constraint [FK_ProductoCategorias_Proyectos] Foreign key (IdProyecto) References Proyectos(Id)
)
