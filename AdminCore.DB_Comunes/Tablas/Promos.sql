﻿CREATE TABLE [dbo].[Promos]
(
	[Id] INT NOT NULL IDENTITY(1,1),
	[Descripcion] varchar(250) NOT NULL,
	[Precio] DECIMAL (18, 2) NOT NULL,
	[IdPromoTipo] int NOT NULL,
	[IdProyecto] INT NOT NULL, 
	[IdImagen] INT NULL, 
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_Promos] Primary Key (Id),
	Constraint [FK_Promos_PromoTipo] Foreign key (IdPromoTipo) References PromoTipo(Id),
	Constraint [FK_Promos_Proyectos] Foreign key (IdProyecto) References Proyectos(Id),
	Constraint [FK_Promos_Imagenes] Foreign key (IdImagen) References Imagenes(Id)
)
