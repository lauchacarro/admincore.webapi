﻿CREATE TABLE [dbo].[MercadoPagoVentas]
(
	[Id] INT NOT NULL IDENTITY (1,1),
	[IdVenta] INT NOT NULL,
	[IdMercadoPagoPreference] VARCHAR(MAX) NOT NULL,
	[IdMercadoPagoCollector] INT NOT NULL DEFAULT 0,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] BIT NOT NULL,
	CONSTRAINT [PK_MercadoPagoVentas] Primary Key (Id ASC),
	CONSTRAINT [FK_MercadoPagoVentas_Ventas] Foreign key (IdVenta) References Ventas(Id)
)
