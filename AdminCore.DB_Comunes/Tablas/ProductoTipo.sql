﻿CREATE TABLE [dbo].[ProductoTipo]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Descripcion] varchar(250) NOT NULL,
	[Activo] bit NOT NULL,
	CONSTRAINT [PK_ProductoTipo] Primary key (Id)
)
