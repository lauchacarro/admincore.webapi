﻿CREATE TABLE [dbo].[VentaProductos]
(
	[Id] INT NOT NULL Identity(1, 1),
	[IdVenta] INT NOT NULL,
	[IdProducto] INT NOT NULL,
	[CantidadUnidades] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] BIT NOT NULL,
	CONSTRAINT [PK_VentaProductos] Primary Key (Id),
	CONSTRAINT [FK_VentaProductos_Venta] Foreign Key (IdVenta) References Ventas(Id),
	CONSTRAINT [FK_VentaProductos_Productos] Foreign Key (IdProducto) References Productos(Id)
)
