﻿CREATE TABLE [dbo].[UsuarioTipo]
(
	[Id] INT NOT NULL Identity(1,1), 
    [Descripcion] VARCHAR(50) NOT NULL,
	[Activo] BIT NOT NULL, 
    Constraint [PK_UsuarioTipo] Primary Key (Id ASC)

)
