﻿CREATE TABLE [dbo].[VentaItems]
(
	[Id] INT NOT NULL Identity(1, 1),
	[IdVenta] INT NOT NULL,
	[IdProducto] INT NULL,
	[IdPromo] INT NULL,
	[CantidadUnidades] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] BIT NOT NULL,
	CONSTRAINT [PK_VentaItems] Primary Key (Id),
	CONSTRAINT [FK_VentaItems_Venta] Foreign Key (IdVenta) References Ventas(Id),
	CONSTRAINT [FK_VentaItems_Productos] Foreign Key (IdProducto) References Productos(Id),
	CONSTRAINT [FK_VentaItems_Promo] Foreign Key (IdPromo) References Promos(Id)
)
