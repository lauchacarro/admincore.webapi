﻿CREATE TABLE [dbo].[Carrousel]
(
	[Id] INT NOT NULL Identity (1,1),
	[IdImagen] int  NOT NULL,
	[Titulo] varchar(250)  NULL,
	[Descripcion] varchar(250) NULL,
	[IdProyecto] INT NOT NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] bit NOT NULL,
	Constraint [PK_Carrousel] Primary Key (Id ASC),
	Constraint [FK_Carrousel_Proyectos] Foreign key (IdProyecto) References Proyectos(Id),
	Constraint [FK_Carrousel_Imagenes] Foreign key (IdImagen) References Imagenes(Id)

)


