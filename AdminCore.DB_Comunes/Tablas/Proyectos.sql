﻿CREATE TABLE [dbo].[Proyectos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[IdProyectoTipo] [int] NOT NULL,
	[GitUrl] [varchar](max) NULL,
	[FechaAlta] DATETIME NOT NULL,
	[Activo] [bit] NOT NULL,
	
    CONSTRAINT [PK_Proyectos] PRIMARY KEY CLUSTERED  ( [Id] ASC ) ,
 CONSTRAINT [FK_Proyectos_Tipo] FOREIGN KEY([IdProyectoTipo]) REFERENCES [dbo].[ProyectoTipo] ([Id])
)
GO

