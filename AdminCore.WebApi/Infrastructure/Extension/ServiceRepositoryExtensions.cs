﻿using AdminCore.Repositories.Abstract;
using AdminCore.Repositories.Concrete;
using AdminCore.Services.Abstract;
using AdminCore.Services.Concrete;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.Infrastructure.Extension
{
    public static class ServiceRepositoryExtensions
    {
        public static IServiceCollection AgregarServiciosYRepositorios(this IServiceCollection services)
        {
            return services.AgregarServicios().AgregarRepositorios();
        }


        static IServiceCollection AgregarServicios(this IServiceCollection services)
        {
            return 
            services.AddTransient<IUsuarioService, UsuarioService>()
            .AddTransient<ICategoriaService, CategoriaService>()
            .AddTransient<ISubCategoriaService, SubCategoriaService>()
            .AddTransient<IProyectoService, ProyectoService>()
            .AddTransient<IProductoService, ProductoService>()

            .AddTransient<IStockService, StockService>()
            .AddTransient<IPromoService, PromoService>()
            .AddTransient<IPromoTemporalService, PromoTemporalService>()
            .AddTransient<IPromoProductoService, PromoProductoService>()

            .AddTransient<ICarrouselService, CarrouselService>()
            .AddTransient<IMercadoPagoCredencialService, MercadoPagoCredencialService>()
            .AddTransient<IMercadoPagoVentaService, MercadoPagoVentaService>()
            .AddTransient<IMercadoPagoService, MercadoPagoService>()
            .AddTransient<IMercadoPagoVentaService, MercadoPagoVentaService>()
            .AddTransient<IVentaItemService, VentaItemService>()
            .AddTransient<IVentaProductoService, VentaProductoService>()
            .AddTransient<IVentaService, VentaService>()
            .AddTransient<IVentaEnvioService, VentaEnvioService>()
            .AddTransient<IImgurService, ImgurService>()
            .AddTransient<IContactoService, ContactoService>()
            .AddTransient<INotificacionService, NotificacionService>()
            .AddTransient<IImagenService, ImagenService>()

            .AddTransient<IVisitaService, VisitaService>();
        }

        static IServiceCollection AgregarRepositorios(this IServiceCollection services)
        {
            return
            services.AddTransient<IUsuarioRepository, UsuarioRepository>()
            .AddTransient<ICategoriaRepository, CategoriaRepository>()
            .AddTransient<ISubCategoriaRepository, SubCategoriaRepository>()
            .AddTransient<IProyectoRepository, ProyectoRepository>()
            .AddTransient<IProductoRepository, ProductoRepository>()

            .AddTransient<IStockRepository, StockRepository>()
            .AddTransient<IPromoTemporalRepository, PromoTemporalRepository>()
            .AddTransient<IPromoRepository, PromoRepository>()

            .AddTransient<ICarrouselRepository, CarrouselRepository>()
            .AddTransient<IPromoProductoRepository, PromoProductoRepository>()
            .AddTransient<IMercadoPagoCredencialRepository, MercadoPagoCredencialRepository>()
            .AddTransient<IMercadoPagoVentaRepository, MercadoPagoVentaRepository>()
            .AddTransient<IVentaItemRepository, VentaItemRepository>()
            .AddTransient<IVentaProductoRepository, VentaProductoRepository>()
            .AddTransient<IVentaRepository, VentaRepository>()
            .AddTransient<IVentaEnvioRepository, VentaEnvioRepository>()
            .AddTransient<IContactoRepository, ContactoRepository>()
            .AddTransient<IImagenRepository, ImagenRepository>()
            .AddTransient<INotificacionRepository, NotificacionRepository>()
            .AddTransient<IVisitaRepository, VisitaRepository>();

        }
    }
}
