﻿using AdminCore.Business.Dto;
using AdminCore.Models;
using AdminCore.Services.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.Infrastructure.Mapping
{
    public class UsuarioProfile : Profile
    {
        public UsuarioProfile()
        {
            CreateMap<LoginViewModel, Usuario>()
            .ForMember(dest =>
                dest.Alias,
                opt => opt.MapFrom(src => src.Alias)
            )
            .ForMember(dest =>
                dest.Contraseña,
                opt => opt.MapFrom(src => src.Contraseña)
            );
        }
    }
}
