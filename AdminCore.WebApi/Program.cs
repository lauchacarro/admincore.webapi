﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AdminCore.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build()


            .Run();
        }

        //public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //    .UseContentRoot(Directory.GetCurrentDirectory())
        //        .UseKestrel()
        //        .UseStartup<Startup>()
        //    .UseUrls("http://localhost:63520");


        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    Console.WriteLine(hostingContext.HostingEnvironment.EnvironmentName);
                    Console.WriteLine(Directory.GetCurrentDirectory());

                    // Call other providers here and call AddCommandLine last.
                    config.AddCommandLine(args);
                    config.SetBasePath(Directory.GetCurrentDirectory());

                    config.AddJsonFile(GetAppSettings(hostingContext.HostingEnvironment), false, true);


                })

                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel()

                .UseStartup<Startup>();
        }

        private static string GetAppSettings(IHostingEnvironment environment)
        {
            if (environment.IsProduction())
            {
                return "appsettings.json";
            }
            else
            {
                return $"appsettings.{environment.EnvironmentName}.json";

            }
        }
    }
}
