﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Stock
{
    public class StockDeleteRequest
    {
        public int Id { get; set; }
    }
}
