﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Stock
{
    public class StockPostRequest
    {
        public int IdProducto { get; set; }

        public int CantidadModificacion { get; set; }

        public int IdStockTipoModificacion { get; set; }
    }
}
