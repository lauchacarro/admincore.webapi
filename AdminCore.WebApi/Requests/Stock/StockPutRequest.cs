﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Stock
{
    public class StockPutRequest
    {
        public int IdStock { get; set; }

        public int CantidadModificacion { get; set; }

        public int IdStockTipoModificacion { get; set; }
    }
}
