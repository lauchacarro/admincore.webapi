﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.MercadoPagoCredenciales
{
    public class MercadoPagoCredencialPutRequest
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
