﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Producto
{
    public class ProductoDeleteRequest
    {
        public int Id { get; set; }
    }
}
