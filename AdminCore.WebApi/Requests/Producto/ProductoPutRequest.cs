﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Producto
{
    public class ProductoPutRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int IdProductoTipo { get; set; }

        public decimal Precio { get; set; }

        public int IdCategoria { get; set; }

        public int? IdSubCategoria { get; set; }
    }
}
