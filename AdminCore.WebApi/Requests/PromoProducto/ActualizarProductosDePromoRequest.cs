﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.PromoProducto
{
    public class ActualizarProductosDePromoRequest
    {
        public int IdPromo { get; set; }

        public ProductoCantidad[] ProductoCantidad { get; set; }
    }
}
