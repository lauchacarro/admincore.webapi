﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.PromoProducto
{
    public class ProductoCantidad
    {
        public int IdProducto { get; set; }
        public int Cantidad { get; set; }
    }
}
