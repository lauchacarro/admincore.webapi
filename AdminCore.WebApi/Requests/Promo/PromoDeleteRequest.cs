﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Promo
{
    public class PromoDeleteRequest
    {
        public int Id { get; set; }
    }
}
