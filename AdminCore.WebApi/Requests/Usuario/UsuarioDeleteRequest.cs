﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Usuario
{
    public class UsuarioDeleteRequest
    {
        public int Id { get; set; }
    }
}
