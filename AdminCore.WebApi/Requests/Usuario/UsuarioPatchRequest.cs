﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Usuario
{
    public class UsuarioPatchRequest
    {
        public int Id { get; set; }

        public string Contraseña { get; set; }
    }
}
