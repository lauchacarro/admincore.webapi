﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Usuario
{
    public class UsuarioPostRequest
    {
        public UsuarioTipoEnum IdUsuarioTipo { get; set; }

        public string Alias { get; set; }

        public string Contraseña { get; set; }

        public ProyectosEnum IdProyecto { get; set; }
    }
}
