﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.SubCategoria
{
    public class SubCategoriaDeleteRequest
    {
        public int Id { get; set; }
    }
}
