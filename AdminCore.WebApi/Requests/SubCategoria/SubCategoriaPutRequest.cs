﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.SubCategoria
{
    public class SubCategoriaPutRequest
    {
        public int Id { get; set; }

        public string Nombre { get; set; }
    }
}
