﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Venta
{
    public class VentaEnvioPostRequest : VentaPostRequest
    {
        public string Direccion { get; set; }
        public string LinkGoogleMaps { get; set; }
    }
}
