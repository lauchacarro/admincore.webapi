﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Venta.Items
{
    public class CantidadPromos
    {
        public int Id { get; set; }

        public int Cantidad { get; set; }
    }
}
