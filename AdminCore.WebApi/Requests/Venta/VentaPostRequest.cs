﻿using AdminCore.Business.Enums;
using AdminCore.WebApi.Requests.Contacto;
using AdminCore.WebApi.Requests.Venta.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Venta
{
    public class VentaPostRequest
    {
        public ContactoPostRequest Contacto { get; set; }
        

        public VentaTipoEnum Tipo { get; set; }
        public VentaEstadosEnum Estado { get; set; }
        public CantidadPromos[] Promos { get; set; }
        public bool ReturnMercadoPagoLink { get; set; }
        public CantidadProductos[] Productos { get; set; }
    }
}
