﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Venta
{
    public class VentaIdRequest
    {
        public int Id { get; set; }
    }
}
