﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Categoria
{
    public class CategoriaDeleteRequest
    {
        public int Id { get; set; }
    }
}
