﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Categoria
{
    public class CategoriaPutRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

    }
}
