﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Carrousel
{
    public class CarrouselPutRequest
    {
        public int Id { get; set; }
        public string Titulo { get; set; }

        public string Descripcion { get; set; }
    }
}
