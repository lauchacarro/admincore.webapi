﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Requests.Auth
{
    public class LoginUsuarioPostRequest
    {
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
    }
}
