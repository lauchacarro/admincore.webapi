﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Responses.Venta
{
    public class VentaResponse
    {
        public string LinkMP { get; set; }

        public int IdVenta { get; set; }
    }
}
