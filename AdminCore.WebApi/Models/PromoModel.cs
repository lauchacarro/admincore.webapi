﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class PromoModel
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public decimal Precio { get; set; }

        public string PromoTipo { get; set; }

        public string FechaInicio { get; set; }

        public string FechaFin { get; set; }

        public string LinkImagen { get; set; }
    }
}
