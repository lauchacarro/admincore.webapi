﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class VentaModel
    {
        public int Id { get; set; }

        public string Estado { get; set; }
        public string Tipo { get; set; }
        public string FechaCambioEstado { get; set; }
        public decimal Monto { get; set; }
        public string[] Productos { get; set; }
        public string[] Promos { get; set; }
        public string LinkMap { get; set; }

        public ContactoModel Contacto { get; set; }


    }
}
