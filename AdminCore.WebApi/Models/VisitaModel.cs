﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class VisitaModel
    {
        public string Proyecto { get; set; }

        public string Ip { get; set; }

        public DateTime FechaAlta { get; set; }
    }
}
