﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class NotificacionModel
    {
        public int Id { get; set; }
        public string Titulo{ get; set; }
        public string Descripcion { get; set; }
        public string FechaAlta { get; set; }
    }
}
