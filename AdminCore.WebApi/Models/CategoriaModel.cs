﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class CategoriaModel
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public int IdProyecto { get; set; }

        public DateTime FechaAlta { get; set; }

    }
}
