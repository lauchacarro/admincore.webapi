﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class MercadoPagoCredencialModel
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
