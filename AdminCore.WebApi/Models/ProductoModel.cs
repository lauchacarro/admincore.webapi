﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class ProductoModel
    {

        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string ProductoTipo { get; set; }

        public decimal Precio { get; set; }

        public int Stock { get; set; }

        public string LinkImagen { get; set; }

        public int IdCategoria { get; set; }

        public string Categoria { get; set; }

        public int IdSubCategoria { get; set; }

        public string SubCategoria { get; set; }

        public DateTime FechaAlta { get; set; }

    }
}
