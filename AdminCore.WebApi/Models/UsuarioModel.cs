﻿using AdminCore.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class UsuarioModel
    {
        public int Id { get; set; }

        public string UsuarioTipo { get; set; }

        public string Alias { get; set; }

       

        public string Proyecto { get; set; }
    }
}
