﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Models
{
    public class StockModel
    {
        public int Id { get; set; }

        public int CantidadModificacion { get; set; }

        public int IdStockTipoModificacion { get; set; }
        public string StockTipoModificacion { get; set; }

        public DateTime FechaAlta { get; set; }
    }
}
