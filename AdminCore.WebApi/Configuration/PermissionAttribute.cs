﻿using AdminCore.Business.Enums;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.Configuration
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionAttribute : AuthorizeAttribute
    {

        //intentar hacer que se ingresen otros tipos de roles
        //public UsuarioTipo UsuarioTipo { get; }

        public UsuarioTipoEnum UsuarioTipo { get; }

        public PermissionAttribute(UsuarioTipoEnum tipoUsuario) : base("Permission")
        {
            //UsuarioTipo = usuarioTipo;
            UsuarioTipo = tipoUsuario;
        }

       
    }
}
