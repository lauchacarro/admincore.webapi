﻿using AdminCore.Business.Enums;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AdminCore.Configuration
{

    //https://code.i-harness.com/es/q/1e01ba7

    public class PermissionAuthorizationHandler : AttributeAuthorizationHandler<PermissionAuthorizationRequirement, PermissionAttribute>
    {
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement, IEnumerable<PermissionAttribute> attributes)
        {

            if(attributes.Count() > 0)
            {
                foreach (var permissionAttribute in attributes)
                {
                    bool response = await AuthorizeAsync(context.User, permissionAttribute.UsuarioTipo);
                    if (!response)
                    {
                        return;
                    }
                }
          
            }
            else
            {
                if(context.User.Claims.Count() == 0)
                {
                    return;
                }
            }
            

            context.Succeed(requirement);
        }

        private Task<bool> AuthorizeAsync(ClaimsPrincipal user, UsuarioTipoEnum usuarioTipo)
        {
            //Aca es donde tengo que buscar el permission en el Claim y si esta entonces estara autorizado devolviendo true


            bool response = false;
            //Pregunto si hay Claims guardados
            if (user.Claims.Count() > 0)
            {
                //Pregunto si alguno de los usuarios tiene un Role que seria el usuario Tipo
                if (user.HasClaim(c => c.Type == ClaimTypes.Role))
                {
                    //Si contienen, entonces verifico que sea el mismo que el de la peticion de autorizacion
                    //Si el usuario es del tipo OmniPotente, entonces podra realizar cualquier accion
                    if (user.Claims.Any(c => c.Value == usuarioTipo.ToString() || c.Value == UsuarioTipoEnum.OmniPotente.ToString()))
                    {
                        response = true;
                    }
                    else
                    {
                        //Si no lo es entonces no esta autorizado
                        response = false;
                    }
                }
                else
                {
                    //Para estar autorizado debe haber un usuario 
                    response = false;
                }

            }
            else
            {
                //Para estar autorizado debe haber un usuario 
                response = false;
            }

            //return new Task<bool>(() => response); 
            return Task.FromResult<bool>(response);

        }


    }
}
