﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.WebApi.Requests.Venta;
using AdminCore.WebApi.Requests.Venta.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Helpers
{
    public class VentaHelper
    {
        public static Venta CrearInstancia(VentaPostRequest request, ProyectosEnum proyecto)
        {
            Venta venta = CrearInstancia(request);
            venta.Proyecto = proyecto;
            return venta;
        }

        public static Venta CrearInstancia(VentaPostRequest request)
        {
            return new Venta()
            {
                Tipo = request.Tipo,
                Estado = request.Estado,
                FechaAlta = DateTime.Now,
                FechaCambioEstado = DateTime.Now,
                Activo = true,
                Monto = 0
            };
        }

        public static Notificacion VentaToNotificacion(Venta venta)
        {
            Notificacion notificacion = new Notificacion();
            switch (venta.Estado)
            {
                case VentaEstadosEnum.Reservada:
                    notificacion.Titulo = "Reservación";
                    notificacion.Descripcion = "Alguien realizó una reservación.";
                    notificacion.Tipo = NotificacionTipoEnum.VentaReservada;
                    break;
                case VentaEstadosEnum.Creada:
                    notificacion.Titulo = "Venta Creada";
                    notificacion.Descripcion = "Se empezó una venta.";
                    break;
                case VentaEstadosEnum.PendientePagar:
                    notificacion.Titulo = "Venta Pendiente Pagar";
                    notificacion.Descripcion = "Se comenzó el pago de una venta.";
                    
                    break;
                case VentaEstadosEnum.Pagada:
                    notificacion.Titulo = "Venta Pagada";
                    notificacion.Descripcion = "Se finalizó el pago de una venta.";
                    notificacion.Tipo = NotificacionTipoEnum.VentaPagada;
                    break;
                case VentaEstadosEnum.PendienteEntrega:
                    notificacion.Titulo = "Venta Pendiente Envio";
                    notificacion.Descripcion = "Se finalizó el pago de la venta. Se debe realizar el envió de los productos.";
                    notificacion.Tipo = NotificacionTipoEnum.VentaParaEnviar;
                    break;
                case VentaEstadosEnum.Finalizada:
                    notificacion.Titulo = "Venta Finalizada";
                    notificacion.Descripcion = "Se finalizó la venta.";
                    break;
                case VentaEstadosEnum.Devuelta:
                    notificacion.Titulo = "Venta Devuelta";
                    notificacion.Descripcion = "La devolución se completó.";
                    break;
                case VentaEstadosEnum.Cancelada:
                    notificacion.Titulo = "Venta Cancelada";
                    notificacion.Descripcion = "Se canceló la venta.";
                    break;
                default:
                    break;
            }

            notificacion.Proyecto = venta.Proyecto;
            return notificacion;
        }


    }
}
