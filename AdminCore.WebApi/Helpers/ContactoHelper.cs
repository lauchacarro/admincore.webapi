﻿using AdminCore.Business.Dto;
using AdminCore.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Helpers
{
    public class ContactoHelper
    {
        public static  ContactoModel EnriquecerModel(Contacto contacto)
        {
            return new ContactoModel()
            {
                Nombre = contacto.Nombre,
                Apellido = contacto.Apellido,
                Mail = contacto.Mail,
                Telefono = contacto.Telefono
            };
        }
    }
}
