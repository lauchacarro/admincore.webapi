﻿using AdminCore.Business.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Helpers
{
    public class UsuarioHelper
    {
        public static Usuario ObtenerUsuario(ClaimsPrincipal claims)
        {
            string jsonUser = claims.FindFirstValue(ClaimTypes.Actor);
            return JsonConvert.DeserializeObject<Usuario>(jsonUser);
        }
    }
}
