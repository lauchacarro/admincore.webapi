﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.SubCategoria;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class SubCategoriasController : ControllerBase
    {
        readonly ISubCategoriaService _service;

        readonly ICategoriaService _categoriaService;

        public SubCategoriasController(ISubCategoriaService service, ICategoriaService categoriaService)
        {
            _service = service;
            _categoriaService = categoriaService;
        }


        [HttpGet("[action]")]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult GetByCategoria(int idCategoria)
        {
            ServiceResponseEnumerableResult<SubCategoria> response = _service.ObtenerSubCategorias(idCategoria);
            if (response.Success)
            {
                IEnumerable<SubCategoria> listaSubCategorias = response.Result;
                return Ok(listaSubCategorias.Select(x => new SubCategoriaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    FechaAlta = x.FechaAlta,
                    IdCategoria = x.IdCategoria
                }));
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }



        [HttpGet]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult Get(int? id)
        {
            if (id.HasValue)
            {

                ServiceResponseResult<SubCategoria> response = _service.ObtenerSubCategoriaPorId(id.Value);

                if (response.Success)
                {
                    SubCategoria subCategoria = response.Result;
                    return Ok(new SubCategoriaModel()
                    {
                        Id = subCategoria.Id,
                        Nombre = subCategoria.Nombre,
                        FechaAlta = subCategoria.FechaAlta,
                        IdCategoria = subCategoria.IdCategoria
                    });
                }
                else
                {
                    return BadRequest(response.Mensage);
                }

            }
            else
            {


                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);

                ServiceResponseEnumerableResult<Categoria> response = _categoriaService.ObtenerCategorias(usuario.Proyecto);

                if (response.Success)
                {
                    List<Categoria> listaCategorias = response.Result.ToList();
                    List<SubCategoria> listaSubCategorias = new List<SubCategoria>();

                    foreach (Categoria categoria in listaCategorias)
                    {
                        listaSubCategorias.AddRange(

                            _service.ObtenerSubCategorias(categoria.Id).Result
                        );
                    }

                    return Ok(listaSubCategorias.Select(x => new SubCategoriaModel()
                    {
                        Id = x.Id,
                        Nombre = x.Nombre,
                        FechaAlta = x.FechaAlta,
                        IdCategoria = x.IdCategoria
                    }));

                }
                else
                {
                    return BadRequest(response.Mensage);
                }


            }
        }

        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post(SubCategoriaPostRequest request)
        {
            SubCategoria _categoria = new SubCategoria()
            {
                Nombre = request.Nombre,
                FechaAlta = DateTime.Now,
                IdCategoria = request.IdCategoria,
                Activo = true
            };
            ServiceResponse response = _service.AgregarSubCategoria(_categoria);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }

        }

        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put(SubCategoriaPutRequest request)
        {

            SubCategoria subCategoria = (SubCategoria)_service.ObtenerSubCategoriaPorId(request.Id).Result;
            subCategoria.Nombre = request.Nombre;


            ServiceResponse response = _service.ModificarSubCategoria(subCategoria);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }

        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(SubCategoriaDeleteRequest request)
        {
            ServiceResponse response = _service.DesactivarSubCategoria(request.Id);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }
    }
}