﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Contacto;
using AdminCore.WebApi.Requests.Venta;
using AdminCore.WebApi.Requests.Venta.Items;
using AdminCore.WebApi.Responses.Venta;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly IVentaService _service;
        private readonly IContactoService _contactoService;
        private readonly IPromoService _promoService;
        private readonly IProductoService _productoService;
        private readonly IVentaItemService _ventaItemService;
        private readonly IVentaEnvioService _ventaEnvioService;
        private readonly IMercadoPagoService _mercadoPagoService;
        private readonly INotificacionService _notificacionService;
        private readonly IMercadoPagoVentaService _mercadoPagoVenta;

        public VentasController(IVentaService service, IContactoService contactoService, IPromoService promoService, IProductoService productoService, IVentaItemService ventaItemService, IVentaEnvioService ventaEnvioService, IMercadoPagoService mercadoPagoService, IMercadoPagoVentaService mercadoPagoVenta)
        {
            _service = service;
            _contactoService = contactoService;
            _promoService = promoService;
            _productoService = productoService;
            _ventaItemService = ventaItemService;
            _ventaEnvioService = ventaEnvioService;
            _mercadoPagoService = mercadoPagoService;
            _mercadoPagoVenta = mercadoPagoVenta;
        }

        [HttpPost("Notificacion")]
        [Route("Notificacion/{idVenta}")]
        public IActionResult Notificacion(int idVenta, string topic, string id)
        {
            if (topic == "payment")
            {
                if (ActualizarVenta(idVenta, id))
                    Ok();
                else
                    NoContent();

            }

            return NoContent();

        }






        [HttpPut("Finalizar")]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Finalizar(VentaIdRequest request)
        {

            Venta venta = _service.ObtenerVentaPorId(request.Id).Result;
            venta.Estado = VentaEstadosEnum.Finalizada;
            _service.ModificarVenta(venta);


            return Ok();
        }

        [HttpPut("PendientePagar")]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult PendientePagar(VentaIdRequest request)
        {

            Venta venta = _service.ObtenerVentaPorId(request.Id).Result;
            venta.Estado = VentaEstadosEnum.PendientePagar;
            _service.ModificarVenta(venta);


            return Ok();
        }

        [HttpPut("Pagar")]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Pagar(VentaIdRequest request)
        {

            Venta venta = _service.ObtenerVentaPorId(request.Id).Result;
            venta.Estado = VentaEstadosEnum.Pagada;
            _service.ModificarVenta(venta);


            return Ok();
        }


        [Permission(UsuarioTipoEnum.OmniPotente)]
        [HttpPut("Cancelar")]
        public IActionResult Cancelar(VentaIdRequest request)
        {

            Venta venta = _service.ObtenerVentaPorId(request.Id).Result;
            venta.Estado = VentaEstadosEnum.Cancelada;
            _service.ModificarVenta(venta);


            return Ok();
        }


        [Permission(UsuarioTipoEnum.OmniPotente)]
        [HttpPut("Devolver")]
        public IActionResult Devolver(VentaIdRequest request)
        {

            Venta venta = _service.ObtenerVentaPorId(request.Id).Result;
            venta.Estado = VentaEstadosEnum.Devuelta;
            _service.ModificarVenta(venta);


            return Ok();
        }


        [Permission(UsuarioTipoEnum.OmniPotente)]
        [HttpPut("Enviar")]
        public IActionResult Enviar(VentaIdRequest request)
        {

            Venta venta = _service.ObtenerVentaPorId(request.Id).Result;
            venta.Estado = VentaEstadosEnum.PendienteEntrega;
            _service.ModificarVenta(venta);


            return Ok();
        }


        [Permission(UsuarioTipoEnum.OmniScience)]
        [HttpGet("GetByEstado")]
        public IActionResult GetByEstado(VentaEstadosEnum estado)
        {
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            var response = _service.ObtenerVentas(usuario.Proyecto, estado);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok(response.Result.Select(x => EnriquecerModel(x)));
        }


        [Permission(UsuarioTipoEnum.OmniScience)]
        [HttpGet]
        public IActionResult Get(int? id)
        {
            if (id.HasValue)
            {
                var response = _service.ObtenerVentaPorId(id.Value);
                if (!response.Success)
                {
                    return BadRequest(response.Mensage);
                }
                return Ok(EnriquecerModel(response.Result));
            }
            else
            {
                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
                var response = _service.ObtenerVentas(usuario.Proyecto);
                if (!response.Success)
                {
                    return BadRequest(response.Mensage);
                }
                List<VentaModel> listaVentaModels = new List<VentaModel>();
                foreach (var venta in response.Result)
                {
                    listaVentaModels.Add(EnriquecerModel(venta));
                }
                return Ok(listaVentaModels);
            }
        }

        [HttpPost("Envio")]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult PostVentaEnvio(VentaEnvioPostRequest request)
        {
            if (!HayItems(request.Productos, request.Promos))
            {
                return BadRequest("No se puede crear una venta sin Items.");
            }
            else
            {
                VentaResponse response = CrearVenta(request, out string msgError);


                if (!string.IsNullOrWhiteSpace(msgError))
                {
                    return BadRequest(msgError);
                }


                CrearVentaEnvio(request, response, out msgError);

                if (!string.IsNullOrWhiteSpace(msgError))
                {
                    return BadRequest(msgError);
                }
                return Ok(response);
            }

        }



        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post(VentaPostRequest request)
        {
            if (!HayItems(request.Productos, request.Promos))
            {
                return BadRequest("No se puede crear una venta sin Items.");
            }
            else
            {
                VentaResponse response = CrearVenta(request, out string msgError);


                if (!string.IsNullOrWhiteSpace(msgError))
                {
                    return BadRequest(msgError);
                }

                return Ok(response);
            }
        }

        private VentaEnvio CrearVentaEnvio(VentaEnvioPostRequest request, VentaResponse response, out string msgError)
        {
            msgError = null;
            VentaEnvio ventaEnvio = new VentaEnvio()
            {
                Direccion = request.Direccion,
                LinkGoogleMaps = request.LinkGoogleMaps,
                Activo = true,
                FechaAlta = DateTime.Now,
                IdVenta = response.IdVenta
            };
            ServiceResponseResult<VentaEnvio> ventaEnvioResponse = _ventaEnvioService.AgregarVentaEnvio(ventaEnvio);
            if (!ventaEnvioResponse.Success)
            {
                msgError = ventaEnvioResponse.Mensage;
                return null;
            }
            return ventaEnvio;
        }
        private VentaResponse CrearVenta(VentaPostRequest request, out string msgError)
        {
            msgError = null;
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            Venta venta = VentaHelper.CrearInstancia(request, usuario.Proyecto);
            Contacto contacto;
            if (request.Contacto != null)
            {
                ServiceResponseResult<Contacto> contactoResponse = CrearContacto(request.Contacto, usuario.Proyecto);
                if (!contactoResponse.Success)
                {
                    msgError = contactoResponse.Mensage;
                    return null;
                }
                contacto = contactoResponse.Result;
                venta.IdContacto = contacto.Id;
            }
            List<VentaItem> Items = EnriquecerListaItems(request.Productos, request.Promos, out decimal montoTotal);
            venta.Monto = montoTotal;

            var ventaResponse = _service.AgregarVenta(venta);
            if (!ventaResponse.Success)
            {
                msgError = ventaResponse.Mensage;
                return null;
            }
            Items.ForEach(x => x.IdVenta = ventaResponse.Result.Id);
            var ventaItemsResponse = _ventaItemService.AgregarVentaItems(Items);
            if (!ventaItemsResponse.Success)
            {
                msgError = ventaItemsResponse.Mensage;
                return null;
            }
            VentaResponse response = new VentaResponse();
            response.IdVenta = venta.Id;
            if (request.ReturnMercadoPagoLink)
            {
                CrearMercadoPagoVenta(venta, out string linkMercadoPago);
                response.LinkMP = linkMercadoPago;
            }
            return response;

        }

        private MercadoPagoVenta CrearMercadoPagoVenta(Venta venta, out string linkMercadoPago)
        {
            var preference = _mercadoPagoService.CrearPreference(venta);
            var response = _mercadoPagoVenta.AgregarMercadoPagoVenta(new MercadoPagoVenta()
            {
                Activo = true,
                FechaAlta = DateTime.Now,
                idMercadoPagoPreference = preference.Id,
                IdVenta = venta.Id,
                IdMercadoPagoCollector = preference.CollectorId.Value

            });



#if DEBUG
            linkMercadoPago = preference.SandboxInitPoint;
#else
                    linkMercadoPago = preference.InitPoint;
#endif
            return response.Result;

        }

        private bool HayItems(IEnumerable<CantidadProductos> productos, IEnumerable<CantidadPromos> promos)
        {
            return ((productos != null || productos.Count() > 0) && (promos != null || promos.Count() > 0));

        }

        private List<VentaItem> EnriquecerListaItems(IEnumerable<CantidadProductos> productos, IEnumerable<CantidadPromos> promos, out decimal montoTotal)
        {
            montoTotal = 0;
            List<VentaItem> Items = new List<VentaItem>();
            foreach (var producto in productos.GroupBy(x => x.Id).Select(x => x.First()))
            {
                VentaItem ventaItem = new VentaItem()
                {
                    CantidadUnidades = productos.Where(x => x.Id == producto.Id).Sum(x => x.Cantidad),
                    IdPromo = null,
                    IdProducto = producto.Id,
                    Activo = true,
                    FechaAlta = DateTime.Now
                };
                Items.Add(ventaItem);
                var productoResponse = _productoService.ObtenerProductoPorId(producto.Id);

                montoTotal += productoResponse.Result.Precio * ventaItem.CantidadUnidades;
            }
            foreach (var promo in promos.GroupBy(x => x.Id).Select(x => x.First()))
            {
                VentaItem ventaItem = new VentaItem()
                {
                    CantidadUnidades = promos.Where(x => x.Id == promo.Id).Sum(x => x.Cantidad),
                    IdPromo = promo.Id,
                    IdProducto = null,
                    Activo = true,
                    FechaAlta = DateTime.Now
                };
                Items.Add(ventaItem);
                var promoResponse = _promoService.ObtenerPromoPorId(promo.Id);

                montoTotal += promoResponse.Result.Precio * ventaItem.CantidadUnidades;
            }
            return Items;
        }

        private VentaModel EnriquecerModel(Venta venta)
        {

            var ventaItems = _ventaItemService.ObtenerIdsItemsDeVenta(venta.Id).Result;
            List<string> nombreProductos = new List<string>();
            List<string> nombrePromos = new List<string>();
            foreach (var item in ventaItems)
            {
                if (item.IdProducto.HasValue && !item.IdPromo.HasValue)
                {
                    nombreProductos.Add(_productoService.ObtenerProductoPorId(item.IdProducto.Value).Result.Nombre);
                }
                else
                {
                    nombrePromos.Add(_promoService.ObtenerPromoPorId(item.IdPromo.Value).Result.Descripcion);
                }
            }


            VentaEnvio ventaEnvio = _ventaEnvioService.ObtenerVentaEnvio(venta.Id).Result;
            VentaModel ventaModel = new VentaModel()
            {
                Id = venta.Id,
                Estado = venta.Estado.ToString(),
                FechaCambioEstado = venta.FechaCambioEstado.ToString("dd/MM/yyyy hh:mm"),
                Monto = venta.Monto,
                Tipo = venta.Tipo.ToString(),
                Productos = nombreProductos.ToArray(),
                Promos = nombrePromos.ToArray()
            };
            if (venta.IdContacto.HasValue)
            {
                Contacto contacto = _contactoService.ObtenerContactoPorId(venta.IdContacto.Value).Result;
                ventaModel.Contacto = ContactoHelper.EnriquecerModel(contacto);
            }
            if (ventaEnvio != null)
            {
                ventaModel.LinkMap = ventaEnvio.LinkGoogleMaps;
            }
            return ventaModel;
        }


        private ServiceResponseResult<Contacto> CrearContacto(ContactoPostRequest request, ProyectosEnum proyecto)
        {
            Contacto contacto = new Contacto()
            {
                Nombre = request.Nombre,
                Apellido = request.Apellido,
                Mail = request.Email,
                Telefono = request.Telefono,
                Proyecto = proyecto,
                Activo = true,
                FechaAlta = DateTime.Now

            };
            return _contactoService.AgregarContacto(contacto);
        }


        private bool ActualizarVenta(int idVenta, string idPayment)
        {
            var payment = _mercadoPagoService.ObtenerPayment(idVenta, Convert.ToInt64(idPayment));
            if (payment != null)
            {
                VentaEstadosEnum estado = VentaEstadosEnum.Reservada;
                switch (payment.Status.Value)
                {
                    case MercadoPago.Common.PaymentStatus.pending:
                        estado = VentaEstadosEnum.PendientePagar;
                        break;
                    case MercadoPago.Common.PaymentStatus.approved:
                        var response = _ventaEnvioService.ObtenerVentaEnvio(idVenta);
                        if (response.Result is null)
                            estado = VentaEstadosEnum.Pagada;
                        else
                            estado = VentaEstadosEnum.PendienteEntrega;
                        break;

                }

                Venta venta = _service.ObtenerVentaPorId(idVenta).Result;
                if (venta != null)
                {
                    if (venta.Estado < estado)
                    {
                        venta.Estado = estado;
                        var response = _service.ModificarVenta(venta);
                        if (response.Success)
                        {
                            var notificacion = VentaHelper.VentaToNotificacion(response.Result);
                            _notificacionService.AgregarNotificacion(notificacion);
                            return true;
                        }
                            
                    }

                }
            }
            return false;
        }
    }
}