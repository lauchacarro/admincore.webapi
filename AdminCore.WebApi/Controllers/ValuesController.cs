﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Services.Abstract;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        INotificacionService notificacionService;
        IHttpContextAccessor httpContextAccessor;

        public ValuesController(INotificacionService notificacionService, IHttpContextAccessor httpContextAccessor)
        {
            this.notificacionService = notificacionService;
            this.httpContextAccessor = httpContextAccessor;
        }


        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpGet("Notificacion")]
        public async Task<IActionResult> Notificacion()
        {
            Notificacion f = new Notificacion();
            f.Titulo = "punto a punto";
            f.Descripcion = "notificacion mandada desde la web api";
            f.Proyecto = Business.Enums.ProyectosEnum.AdminCore;
            await notificacionService.AgregarNotificacion(f);
            return Ok();
        }

        [HttpGet("Token")]
        public async Task<IActionResult> Token()
        {
            return Ok(await httpContextAccessor.HttpContext.GetTokenAsync("access_token"));
        }

    }
}
