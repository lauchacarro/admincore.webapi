﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Promo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromosController : ControllerBase
    {
        readonly IPromoService _service;
        readonly IPromoTemporalService _promoTemporalService;
        readonly IPromoProductoService _promoProductoService;
        readonly IImagenService _imagenService;
        readonly IImgurService _imgurService;

        public PromosController(IPromoService service, IPromoTemporalService promoTemporalService, IPromoProductoService promoProductoService, IImagenService imagenService, IImgurService imgurService)
        {
            _service = service;
            _promoTemporalService = promoTemporalService;
            _promoProductoService = promoProductoService;
            _imagenService = imagenService;
            _imgurService = imgurService;
        }

        [HttpGet]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult Get(int? id, bool conImagenes = false)
        {
            if (id.HasValue)
            {
                ServiceResponseResult<Promo> response = _service.ObtenerPromoPorId(id.Value);

                if (!response.Success)
                {
                    return BadRequest(response.Mensage);
                }


                PromoTemporal promoTemporal = null;
                if (response.Result.Tipo == PromoTipoEnum.Temporal)
                {
                    ServiceResponseResult<PromoTemporal> responsePromoTemporal = _promoTemporalService.ObtenerPromoTemporalPorPromo(id.Value);
                    if (!responsePromoTemporal.Success)
                    {
                        return BadRequest(responsePromoTemporal.Mensage);
                    }
                    promoTemporal = responsePromoTemporal.Result;
                }
                if (conImagenes && response.Result.IdImagen.HasValue)
                {
                    ServiceResponseResult<Imagen> responseImagen = (conImagenes) ? _imagenService.ObtenerImagenPorId(response.Result.IdImagen.Value) : null;
                    if (!responseImagen.Success && conImagenes)
                    {
                        return BadRequest(responseImagen.Mensage);
                    }

                    return Ok(EnriquecerModelo(response.Result, promoTemporal, responseImagen.Result));
                }
                else
                {
                    return Ok(EnriquecerModelo(response.Result, promoTemporal));
                }


            }
            else
            {
                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
                List<PromoModel> listaPromoModels = new List<PromoModel>();

                ServiceResponseEnumerableResult<Promo> response = _service.ObtenerPromos(usuario.Proyecto);
                if (!response.Success)
                {
                    return BadRequest(response.Mensage);
                }
                foreach (Promo promo in response.Result)
                {
                    PromoTemporal promoTemporal = null;
                    if (promo.Tipo == PromoTipoEnum.Temporal)
                    {
                        ServiceResponseResult<PromoTemporal> responsePromoTemporal = _promoTemporalService.ObtenerPromoTemporalPorPromo(promo.Id);
                        if (!responsePromoTemporal.Success)
                        {
                            return BadRequest(responsePromoTemporal.Mensage);
                        }
                        promoTemporal = responsePromoTemporal.Result;
                    }
                    if (conImagenes && promo.IdImagen.HasValue)
                    {
                        ServiceResponseResult<Imagen> responsePromoImagen = _imagenService.ObtenerImagenPorId(promo.IdImagen.Value);
                        if (!responsePromoImagen.Success)
                        {
                            return BadRequest(responsePromoImagen.Mensage);
                        }

                        listaPromoModels.Add(EnriquecerModelo(promo, promoTemporal, responsePromoImagen.Result));
                    }
                    else
                    {
                        listaPromoModels.Add(EnriquecerModelo(promo, promoTemporal));
                    }

                }


                return Ok(listaPromoModels);

            }

        }

        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post([FromForm]PromoPostRequest request, IList<IFormFile> files)
        {

            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            Promo promo = new Promo()
            {
                Descripcion = request.Descripcion,
                Tipo = (PromoTipoEnum)request.IdPromoTipo,
                Precio = request.Precio,
                Proyecto = usuario.Proyecto,
                Activo = true,
                FechaAlta = DateTime.Now
            };

            if (files.Count > 0)
            {
                IFormFile img = files[0];
                MemoryStream ms = new MemoryStream();
                img.OpenReadStream().CopyTo(ms);
                Imgur.API.Models.IImage imgurImage = _imgurService.SubirImagenAsync(ms.ToArray()).GetAwaiter().GetResult();
                Imagen productoImagen = new Imagen
                {
                    Link = imgurImage.Link,
                    Stream = ms.ToArray(),
                    FechaAlta = DateTime.Now,
                    Activo = true
                };
                ServiceResponseResult<Imagen> responseImagen = _imagenService.AgregarImagen(productoImagen);
                if (!responseImagen.Success)
                {
                    return BadRequest(responseImagen.Mensage);
                }
                promo.IdImagen = responseImagen.Result.Id;
            }
            ServiceResponseResult<Promo> responsePromo = _service.AgregarPromo(promo);
            if (!responsePromo.Success)
            {
                return BadRequest(responsePromo.Mensage);
            }

            if (request.IdPromoTipo == (int)PromoTipoEnum.Temporal && request.FechaInicio != null & request.FechaFin != null)
            {
                PromoTemporal promoTemporal = new PromoTemporal()
                {
                    FechaInicio = request.FechaInicio.Value,
                    FechaFin = request.FechaFin.Value,
                    IdPromo = responsePromo.Result.Id,
                    Activo = true,
                    FechaAlta = DateTime.Now
                };
                ServiceResponseResult<PromoTemporal> responsePromoTemporal = _promoTemporalService.AgregarPromoTemporal(promoTemporal);
                if (!responsePromoTemporal.Success)
                {
                    return BadRequest(responsePromoTemporal.Mensage);
                }
            }




            return Ok();
        }

        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put([FromForm]PromoPutRequest request, IList<IFormFile> files)
        {
            ServiceResponseResult<Promo> responsePromo = _service.ObtenerPromoPorId(request.Id);
            bool hayCambioPromoTipo;

            if (!responsePromo.Success)
            {
                return BadRequest(responsePromo.Mensage);
            }
            Promo promo = responsePromo.Result;
            promo.Descripcion = request.Descripcion;
            promo.Precio = request.Precio;

            hayCambioPromoTipo = promo.Tipo != (PromoTipoEnum)request.IdPromoTipo;
            promo.Tipo = (PromoTipoEnum)request.IdPromoTipo;


            


            if (promo.Tipo == PromoTipoEnum.Permanente && hayCambioPromoTipo)
            {
                ServiceResponseResult<PromoTemporal> responsePromoTemporal = _promoTemporalService.ObtenerPromoTemporalPorPromo(request.Id);
                if (!responsePromoTemporal.Success)
                {
                    return BadRequest(responsePromoTemporal.Mensage);
                }
                responsePromoTemporal.Result.Activo = false;
                _promoTemporalService.ModificarPromoTemporal(responsePromoTemporal.Result);
            }
            else if (promo.Tipo == PromoTipoEnum.Temporal && request.FechaInicio != null & request.FechaFin != null)
            {
                ServiceResponseResult<PromoTemporal> responsePromoTemporal = _promoTemporalService.ObtenerPromoTemporalPorPromo(request.Id);
                if (!responsePromoTemporal.Success)
                {
                    return BadRequest(responsePromoTemporal.Mensage);
                }
                PromoTemporal promoTemporal = responsePromoTemporal.Result ?? new PromoTemporal();
                promoTemporal.FechaInicio = request.FechaInicio.Value;
                promoTemporal.FechaFin = request.FechaFin.Value;
                promoTemporal.IdPromo = request.Id;
                promoTemporal.Activo = true;
                promoTemporal.FechaAlta = DateTime.Now;
                if (hayCambioPromoTipo || responsePromoTemporal.Result is null)
                {
                    responsePromoTemporal = _promoTemporalService.AgregarPromoTemporal(promoTemporal);
                    if (!responsePromoTemporal.Success)
                    {
                        return BadRequest(responsePromoTemporal.Mensage);
                    }
                }
                else
                {
                    responsePromoTemporal = _promoTemporalService.ModificarPromoTemporal(promoTemporal);
                    if (!responsePromoTemporal.Success)
                    {
                        return BadRequest(responsePromoTemporal.Mensage);
                    }
                }
            }




            if (files.Count > 0)
            {
                ServiceResponseResult<Imagen> responseImagen = _imagenService.ObtenerImagenPorId(promo.IdImagen.HasValue ? promo.IdImagen.Value : 0);

                if (!responseImagen.Success)
                {
                    return BadRequest(responseImagen.Mensage);
                }

                IFormFile img = files[0];
                MemoryStream ms = new MemoryStream();
                img.OpenReadStream().CopyTo(ms);
                Imgur.API.Models.IImage imgurImage = _imgurService.SubirImagenAsync(ms.ToArray()).GetAwaiter().GetResult();
                if (responseImagen.Result is null)
                {
                    Imagen promoImagen = new Imagen
                    {
                        Link = imgurImage.Link,
                        Stream = ms.ToArray(),
                        FechaAlta = DateTime.Now,
                        Activo = true
                    };
                    responseImagen = _imagenService.AgregarImagen(promoImagen);
                    promo.IdImagen = responseImagen.Result.Id;
                }
                else
                {
                    Imagen imagen = responseImagen.Result;

                    imagen.Link = imgurImage.Link;
                    imagen.Stream = ms.ToArray();


                    responseImagen = _imagenService.ModificarImagen(imagen);


                }

                if (!responseImagen.Success)
                {
                    return BadRequest(responseImagen.Mensage);
                }


            }
            responsePromo = _service.ModificarPromo(promo);
            if (!responsePromo.Success)
            {
                return BadRequest(responsePromo.Mensage);
            }
            return Ok();
        }

        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(PromoDeleteRequest request)
        {
            ServiceResponseResult<Promo> response = _service.DesactivarPromo(request.Id);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }



        private PromoModel EnriquecerModelo(Promo promo, PromoTemporal promoTemporal = null, Imagen imagen = null)
        {
            PromoModel promoModel = new PromoModel()
            {
                Id = promo.Id,
                Descripcion = promo.Descripcion,
                PromoTipo = promo.Tipo.ToString(),
                Precio = promo.Precio
            };
            if (promoTemporal != null)
            {
                promoModel.FechaInicio = promoTemporal.FechaInicio.ToString("yyyy-MM-dd");
                promoModel.FechaFin = promoTemporal.FechaFin.ToString("yyyy-MM-dd");
            }
            if (imagen != null)
            {
                promoModel.LinkImagen = imagen.Link;
            }

            return promoModel;
        }
    }
}