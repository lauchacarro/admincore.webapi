﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Usuario;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        readonly IUsuarioService _service;
        public UsuariosController(IUsuarioService service)
        {
            _service = service;
        }

        [HttpGet]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult Get(int? id)
        {
            if (id.HasValue)
            {
                ServiceResponseResult<Usuario> response = _service.ObtenerPorId(id.Value);
                if (!response.Success)
                {
                    return BadRequest(response.Mensage);
                }
                return Ok(EnriquezerModel(response.Result));
            }
            else
            {
                ServiceResponseEnumerableResult<Usuario> response = _service.ObtenerUsuarios();
                if (!response.Success)
                {
                    return BadRequest(response.Mensage);
                }
                return Ok(response.Result.Select(x=> EnriquezerModel(x)));
            }
        }

        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post(UsuarioPostRequest request)
        {
            Usuario usuario = new Usuario()
            {
                Alias = request.Alias,
                Contraseña = request.Contraseña,
                Proyecto = request.IdProyecto,
                Tipo = request.IdUsuarioTipo,
                FechaAlta = DateTime.Now,
                Activo = true
            };
            ServiceResponseResult<Usuario> response =  _service.AgregarUsuario(usuario);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok();
        }

        [HttpPatch]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Patch(UsuarioPatchRequest request)
        {
            ServiceResponseResult<Usuario> response = _service.CambiarContraseñaUsuario(request.Id, request.Contraseña);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok();
        }

        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(UsuarioDeleteRequest request)
        {
            ServiceResponseResult<Usuario> response = _service.DesactivarUsuario(request.Id);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok();
        }

        private UsuarioModel EnriquezerModel(Usuario usuarioDto)
        {
            return new UsuarioModel()
            {
                Alias = usuarioDto.Alias,
                Id = usuarioDto.Id,
                Proyecto = usuarioDto.Proyecto.ToString(),
                UsuarioTipo = usuarioDto.Tipo.ToString()
            };
        } 
    }
}