﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Producto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        readonly IProductoService _service;
        readonly IImagenService _imageService;
        readonly IStockService _stockService;
        readonly ICategoriaService _categoriaService;
        readonly ISubCategoriaService _subCategoriaService;


        public ProductosController(IProductoService service, IImagenService imageService, IStockService stockService, ICategoriaService categoriaService, ISubCategoriaService subCategoriaService)
        {
            _service = service;
            _imageService = imageService;
            _stockService = stockService;
            _categoriaService = categoriaService;
            _subCategoriaService = subCategoriaService;
        }

        [HttpGet]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult Get(int? id, bool conImagenes)
        {
            if (id.HasValue)
            {
                ServiceResponseResult<Producto> response = _service.ObtenerProductoPorId(id.Value);


                if (response.Success)
                {
                    if (response.Result != null)
                    {
                        return Ok(EnriquezerModelo(response.Result, conImagenes));
                    }
                    else
                    {
                        return BadRequest("No existe el producto.");
                    }

                }
                else
                {
                    return BadRequest(response.Mensage);
                }
            }
            else
            {
                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
                ServiceResponseEnumerableResult<Producto> response = _service.ObtenerProductos(usuario.Proyecto);

                if (response.Success)
                {
                    List<Producto> listaProductos = (List<Producto>)response.Result;
                    List<ProductoModel> listaProductoModels = new List<ProductoModel>();
                    foreach (Producto producto in listaProductos)
                    {

                        listaProductoModels.Add(EnriquezerModelo(producto, conImagenes));



                    }
                    return Ok(listaProductoModels);
                }
                else
                {
                    return BadRequest(response.Mensage);
                }
            }

        }
        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post([FromForm]ProductoPostRequest request, List<IFormFile> files)
        {
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            Producto producto = new Producto()
            {
                Nombre = request.Nombre,
                Descripcion = request.Descripcion,
                IdProductoTipo = (ProductoTipoEnum)request.IdProductoTipo,
                Precio = request.Precio,
                IdCategoria = request.IdCategoria,
                IdSubCategoria = request.IdSubCategoria.Value,
                Activo = true,
                FechaAlta = DateTime.Now,
                Proyecto = usuario.Proyecto,
                Stock = request.Stock
            };
            if (request.IdSubCategoria.Value == 0) producto.IdSubCategoria = null;

            if (files.Count > 0)
            {
                IFormFile img = files[0];
                MemoryStream ms = new MemoryStream();
                img.OpenReadStream().CopyTo(ms);

                ServiceResponseResult<Imagen> responseImagen = _imageService.AgregarImagen(ms.ToArray());
                if (!responseImagen.Success)
                {
                    return BadRequest(responseImagen.Mensage);
                }
                producto.IdImagen = responseImagen.Result.Id;
            }

            ServiceResponseResult<Producto> responseProducto = _service.AgregarProducto(producto);

            if (!responseProducto.Success)
            {
                return BadRequest(responseProducto.Mensage);
            }

            Stock stock = new Stock()
            {
                CantidadModificacion = producto.Stock,
                IdProducto = producto.Id,
                TipoModificacion = StockTipoModificacionEnum.Compra,
                FechaAlta = DateTime.Now,
                Activo = true
            };

            ServiceResponseResult<Stock> responseStock = _stockService.AgregarStock(stock);
            if (!responseStock.Success)
            {
                return BadRequest(responseStock.Mensage);
            }







            return Ok();


        }

        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put([FromForm]ProductoPutRequest request, List<IFormFile> files)
        {
            Producto producto = _service.ObtenerProductoPorId(request.Id).Result;
            producto.Nombre = request.Nombre;
            producto.Descripcion = request.Descripcion;
            producto.IdProductoTipo = (ProductoTipoEnum)request.IdProductoTipo;
            producto.Precio = request.Precio;
            producto.IdCategoria = request.IdCategoria;
            producto.IdSubCategoria = request.IdSubCategoria == 0 ? null : request.IdSubCategoria;
            
            if (files.Count > 0)
            {
                ServiceResponseResult<Imagen> responseImagen = _imageService.ObtenerImagenPorId(producto.IdImagen.HasValue ? producto.IdImagen.Value : 0);
                if (!responseImagen.Success)
                {
                    return BadRequest(responseImagen.Mensage);
                }
                IFormFile img = files[0];
                MemoryStream ms = new MemoryStream();
                img.OpenReadStream().CopyTo(ms);
                Imagen imagen = responseImagen.Result;
                if (responseImagen.Result == null)
                {

                    responseImagen = _imageService.AgregarImagen(ms.ToArray());
                    if (!responseImagen.Success)
                    {
                        return BadRequest(responseImagen.Mensage);
                    }
                    producto.IdImagen = responseImagen.Result.Id;
                }
                else
                {
                    responseImagen = _imageService.ModificarImagen(imagen.Id, ms.ToArray());
                }


                if (!responseImagen.Success)
                {
                    return BadRequest(responseImagen.Mensage);
                }
            }
            ServiceResponseResult<Producto> responseProducto = _service.ModificarProducto(producto);
            if (!responseProducto.Success)
            {
                return BadRequest(responseProducto.Mensage);
            }
            return Ok();

        }
        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(ProductoDeleteRequest request)
        {
            ServiceResponseResult<Producto> response = _service.DesactivarProducto(request.Id);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }

        private ProductoModel EnriquezerModelo(Producto producto, bool conImagenes)
        {
            ServiceResponseResult<Categoria> responseCategoria = _categoriaService.ObtenerCategoriaPorId(producto.IdCategoria);



            ProductoModel productoModel = new ProductoModel()
            {
                Id = producto.Id,
                Categoria = responseCategoria.Result.Nombre,
                IdCategoria = producto.IdCategoria,
                Nombre = producto.Nombre,
                Descripcion = producto.Descripcion,
                Stock = producto.Stock,
                Precio = producto.Precio,
                FechaAlta = producto.FechaAlta,
                ProductoTipo = producto.IdProductoTipo.ToString()
            };

            if (producto.IdSubCategoria.HasValue)
            {
                ServiceResponseResult<SubCategoria> responseSubCategoria = _subCategoriaService.ObtenerSubCategoriaPorId(producto.IdSubCategoria.Value);
                if (responseCategoria.Success)
                {
                    productoModel.IdSubCategoria = producto.IdSubCategoria.Value;
                    productoModel.SubCategoria = responseSubCategoria.Result.Nombre;
                }


            }

            if (conImagenes)
            {
                if (producto.IdImagen.HasValue)
                {
                    ServiceResponseResult<Imagen> responseImagen = _imageService.ObtenerImagenPorId(producto.IdImagen.Value);
                    if (responseImagen.Result != null)
                    {
                        productoModel.LinkImagen = responseImagen.Result.Link;
                    }
                }

            }

            return productoModel;




        }
    }
}