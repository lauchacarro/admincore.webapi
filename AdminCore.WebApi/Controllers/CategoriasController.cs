﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Categoria;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class CategoriasController : ControllerBase
    {
        readonly ICategoriaService _service;

        public CategoriasController(ICategoriaService service)
        {
            _service = service;
        }

        [Permission(UsuarioTipoEnum.OmniScience)]
        [HttpGet]
        public IActionResult Get(int? id)
        {



            if (!id.HasValue)
            {
                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
                ServiceResponseEnumerableResult<Categoria> response = _service.ObtenerCategorias(usuario.Proyecto);

                if (response.Success)
                {
                    List<Categoria> listaCategorias = (List<Categoria>)response.Result;
                    return Ok(listaCategorias.Select(x => new CategoriaModel()
                    {
                        Id = x.Id,
                        Nombre = x.Nombre,
                        FechaAlta = x.FechaAlta,
                        IdProyecto = (int)x.Proyecto
                    }));
                }
                else
                {
                    return BadRequest(response.Mensage);
                }

            }
            else
            {
                ServiceResponseResult<Categoria> response = _service.ObtenerCategoriaPorId(id.Value);
                if (response.Success)
                {

                    CategoriaModel categoria = new CategoriaModel()
                    {
                        Id = response.Result.Id,
                        Nombre = response.Result.Nombre
                    };
                    return Ok(categoria);

                }
                else
                {
                    return BadRequest(response.Mensage);
                }


            }
        }

        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post(CategoriaPostRequest request)
        {
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);

            Categoria _categoria = new Categoria()
            {
                Nombre = request.Nombre,
                FechaAlta = DateTime.Now,
                Proyecto = usuario.Proyecto,
                Activo = true
            };
            ServiceResponse response = _service.AgregarCategoria(_categoria);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }

        }

        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put(CategoriaPutRequest request)
        {

            Categoria categoria = (Categoria)_service.ObtenerCategoriaPorId(request.Id).Result;
            categoria.Nombre = request.Nombre;


            ServiceResponse response = _service.ModificarCategoria(categoria);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }

        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(CategoriaDeleteRequest request)
        {
            ServiceResponse response = _service.DesactivarCategoria(request.Id);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }

    }
}