﻿using AdminCore.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/imagen")]
    [ApiController]
    public class ImagenController : Controller
    {
        private readonly IImagenService _service;

        public ImagenController(IImagenService service)
        {
            _service = service;
        }

        public  IActionResult SincronizarAImgurAsync()

        {
             _service.SincronizarAImgur();
            return Ok();
        }
    }
}
