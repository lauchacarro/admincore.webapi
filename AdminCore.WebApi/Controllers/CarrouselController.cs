﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Carrousel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class CarrouselController : ControllerBase
    {

        readonly ICarrouselService _service;
        readonly IImagenService _imagenService;

        public CarrouselController(ICarrouselService service, IImagenService imagenService)
        {
            _service = service;
            _imagenService = imagenService;
        }

        [Permission(UsuarioTipoEnum.OmniScience)]
        [HttpGet]
        public IActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
                ServiceResponseEnumerableResult<Carrousel> response = _service.ObtenerCarrouseles(usuario.Proyecto);

                if (response.Success)
                {
                    List<Carrousel> listaCarrousel = response.Result;

                    return Ok(response.Result.Select(x => EnriquezerModelo(x)));
                }
                else
                {
                    return BadRequest(response.Mensage);
                }

            }
            else
            {
                ServiceResponseResult<Carrousel> response = _service.ObtenerCarrouselPorId(id.Value);
                if (response.Success)
                {
                    return Ok(EnriquezerModelo(response.Result));

                }
                else
                {
                    return BadRequest(response.Mensage);
                }


            }
        }


        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post([FromForm]CarrouselPostRequest request, IList<IFormFile> files)
        {

            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            IFormFile img = files[0];
            MemoryStream ms = new MemoryStream();
            img.OpenReadStream().CopyTo(ms);
            Carrousel carrousel = new Carrousel
            {
                //Imagen = ms.ToArray(),
                Titulo = request.Titulo,
                Descripcion = request.Descripcion,
                Proyecto = usuario.Proyecto,
                FechaAlta = DateTime.Now,
                Activo = true
            };
            ServiceResponseResult<Imagen> imagenResponse = _imagenService.AgregarImagen(ms.ToArray());
            if (!imagenResponse.Success)
            {
                return BadRequest(imagenResponse.Mensage);
            }
            carrousel.IdImagen = imagenResponse.Result.Id;
            ServiceResponseResult<Carrousel> response = _service.AgregarCarrousel(carrousel);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok();
        }

        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put([FromForm]CarrouselPutRequest request, IList<IFormFile> files)
        {
            ServiceResponseResult<Carrousel> response = _service.ObtenerCarrouselPorId(request.Id);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            Carrousel carrousel = response.Result;
            carrousel.Titulo = request.Titulo;
            carrousel.Descripcion = request.Descripcion;

            if (files.Count > 0)
            {
                IFormFile img = files[0];
                MemoryStream ms = new MemoryStream();
                img.OpenReadStream().CopyTo(ms);

                ServiceResponseResult<Imagen> imagenResponse = _imagenService.ModificarImagen(carrousel.IdImagen, ms.ToArray());
                if (!imagenResponse.Success)
                {
                    return BadRequest(imagenResponse.Mensage);
                }
                carrousel.IdImagen = imagenResponse.Result.Id;
            }


            response = _service.ModificarCarrousel(carrousel);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok();
        }

        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(CarrouselDeleteRequest request)
        {
            ServiceResponseResult<Carrousel> response = _service.DesactivarCarrousel(request.Id);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }

        private CarrouselModel EnriquezerModelo(Carrousel carrousel)
        {
            Imagen imagen = _imagenService.ObtenerImagenPorId(carrousel.IdImagen).Result;
            return new CarrouselModel()
            {
                Id = carrousel.Id,
                Descripcion = carrousel.Descripcion,
                Titulo = carrousel.Titulo,
                LinkImagen = imagen.Link
            };
        }
    }
}
