﻿using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificacionController : ControllerBase
    {
        readonly INotificacionService _service;

        public NotificacionController(INotificacionService service)
        {
            _service = service;
        }

        [Permission(UsuarioTipoEnum.OmniScience)]
        [HttpGet]
        public IActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
                ServiceResponseEnumerableResult<Notificacion> response = _service.ObtenerNotificaciones(usuario.Proyecto);

                if (response.Success)
                {
                    List<Notificacion> listaCarrousel = response.Result;

                    return Ok(response.Result.Select(x => EnriquezerModelo(x)));
                }
                else
                {
                    return BadRequest(response.Mensage);
                }

            }
            else
            {
                ServiceResponseResult<Notificacion> response = _service.ObtenerNotificacionPorId(id.Value);
                if (response.Success)
                {
                    return Ok(EnriquezerModelo(response.Result));

                }
                else
                {
                    return BadRequest(response.Mensage);
                }


            }
        }

        private NotificacionModel EnriquezerModelo(Notificacion x)
        {
            return new NotificacionModel()
            {
                Id = x.Id,
                Descripcion = x.Descripcion,
                Titulo = x.Titulo,
                FechaAlta = x.FechaAlta.ToString("dd/MM/yyyy HH:mm")
            };
        }
    }
}
