﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.MercadoPagoCredenciales;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MercadoPagoCredencialController : ControllerBase
    {
        private readonly IMercadoPagoCredencialService _service;

        public MercadoPagoCredencialController(IMercadoPagoCredencialService service)
        {
            _service = service;
        }

        [Permission(UsuarioTipoEnum.OmniPotente)]
        [HttpGet]
        public IActionResult Get()
        {
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            ServiceResponseResult<MercadoPagoCredencial> response = _service.ObtenerCredencial(usuario.Proyecto);
            if (response.Success)
            {

                return Ok(new MercadoPagoCredencialModel() { ClientId = response.Result.ClientId, ClientSecret = response.Result .ClientSecret});
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }



        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put(MercadoPagoCredencialPutRequest request)
        {
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            ServiceResponseResult<MercadoPagoCredencial> response = _service.ObtenerCredencial(usuario.Proyecto);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            if(response.Result != null)
            {
                MercadoPagoCredencial credencial = response.Result;
                credencial.ClientId = request.ClientId;
                credencial.ClientSecret = request.ClientSecret;
                response = _service.ModificarCredencial(credencial);
            }
            else
            {
                MercadoPagoCredencial credencial = new MercadoPagoCredencial()
                {
                    ClientId = request.ClientId,
                    ClientSecret = request.ClientSecret,
                    FechaAlta = DateTime.Now,
                    Proyecto = usuario.Proyecto,
                    Activo = true
                };
                response = _service.AgregarCredencial(credencial);
            }
            
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }
        }
    }
}