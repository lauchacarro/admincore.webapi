﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Helpers;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Stock;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocksController : ControllerBase
    {
        readonly IStockService _service;

        readonly IProductoService _productoService;

        public StocksController(IStockService service, IProductoService productoService)
        {
            _service = service;
            _productoService = productoService;
        }

        [HttpGet("[action]")]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult GetByProducto(int idProducto)
        {
          
                ServiceResponseEnumerableResult<Stock> response = _service.ObtenerStocks(idProducto);
                if (response.Success)
                {
         
                    return Ok(response.Result.Select(x => EnriquecerModelo(x)));
                }
                else
                {
                    return BadRequest(response.Mensage);
                }
            
        }
        [HttpGet]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult Get(int? id)
        {
            if (id.HasValue)
            {
                ServiceResponseResult<Stock> response = _service.ObtenerStockPorId(id.Value);
                if (response.Success)
                {

                    return Ok(EnriquecerModelo(response.Result));
                }
                else
                {
                    return BadRequest(response.Mensage);
                }
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Post(StockPostRequest request)
        {
            Stock stock = new Stock()
            {
                IdProducto = request.IdProducto,
                CantidadModificacion = request.CantidadModificacion,
                TipoModificacion = (StockTipoModificacionEnum)request.IdStockTipoModificacion,
                Activo = true,
                FechaAlta = DateTime.Now

            };

            ServiceResponseResult<Stock> response = _service.AgregarStock(stock);
            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }

        }
        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Put(StockPutRequest request)
        {
            ServiceResponseResult<Stock> response = _service.ObtenerStockPorId(request.IdStock);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            Stock stock = response.Result;
            stock.CantidadModificacion = request.CantidadModificacion;
            stock.TipoModificacion = (StockTipoModificacionEnum)request.IdStockTipoModificacion;


            response = _service.ModificarStock(stock);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            return Ok();

        }

        [HttpDelete]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult Delete(StockDeleteRequest request)
        {
            ServiceResponseResult<Stock> response = _service.DesactivarStock(request.Id);

            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Mensage);
            }

        }

        private StockModel EnriquecerModelo(Stock stockDto)
        {
            StockModel productoModel = new StockModel()
            {
                Id = stockDto.Id,
                IdStockTipoModificacion = (int)stockDto.TipoModificacion,
                StockTipoModificacion = stockDto.TipoModificacion.ToString(),
                CantidadModificacion = stockDto.CantidadModificacion,
                FechaAlta = stockDto.FechaAlta
                
            };
            return productoModel;
        }
    }
}