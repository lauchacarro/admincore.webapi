﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AdminCore.Business.Dto;
using AdminCore.Repositories.Context;
using AdminCore.Services.Abstract;
using AdminCore.Business.Enums;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Helpers;
using AdminCore.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace AdminCore.Controllers
{
    [Route("api/[controller]/[action]")]
    
    [ApiController]
    public class VisitasController : ControllerBase
    {
        private readonly IVisitaService _visitasService;

        public VisitasController(IVisitaService visitasService)
        {
            _visitasService = visitasService;
        }

        //  api/Visitas/GetVisitas
        [HttpPost]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IEnumerable<VisitaModel> GetVisitas()
        {
            Usuario usuario = UsuarioHelper.ObtenerUsuario(HttpContext.User);
            IEnumerable<Visita> listaVisitas = _visitasService.ObtenerVisitas(usuario.Proyecto);
            return listaVisitas.Select(x => new VisitaModel() { Ip = x.Ip, FechaAlta = x.FechaAlta , Proyecto = x.Proyecto.ToString()}).OrderByDescending(x=> x.FechaAlta);
        }

        [HttpGet]
        [AllowAnonymous]
        public void AddVisita(ProyectosEnum IdProyecto, string ip)
        {
            _visitasService.Agregar(IdProyecto, ip);
        }
    }
}