﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Business.Enums;
using AdminCore.Configuration;
using AdminCore.Services.Abstract;
using AdminCore.Services.Validation;
using AdminCore.WebApi.Requests.Promo;
using AdminCore.WebApi.Requests.PromoProducto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromoProductosController : ControllerBase
    {
        readonly IPromoProductoService _promoProductoService;

        public PromoProductosController(IPromoProductoService promoProductoService)
        {
            _promoProductoService = promoProductoService;
        }

        [HttpGet]
        [Permission(UsuarioTipoEnum.OmniScience)]
        public IActionResult Get(int idPromo)
        {
            var response = _promoProductoService.ObtenerIdsProductosDePromo(idPromo);
            if (response.Success)
            {
                return Ok(response.Result ?? new List<PromoProducto>());
            }
            else
            {
                return BadRequest(response.Mensage);
            }
            
        }

        [HttpPut]
        [Permission(UsuarioTipoEnum.OmniPotente)]
        public IActionResult ActualizarProductosDePromo(ActualizarProductosDePromoRequest request)
        {
            //TODO: Cambiar Logica para poder tener en cuanta la cantidad de unidades de un producto estan asignadas a una promo
            ServiceResponseEnumerableResult<PromoProducto> response = _promoProductoService.ObtenerIdsProductosDePromo(request.IdPromo);
            if (!response.Success)
            {
                return BadRequest(response.Mensage);
            }
            List<ProductoCantidad> listaProductoCantidadToAdd = new List<ProductoCantidad>();
            List<int> listaIdProductoToDelete = new List<int>();
            List<ProductoCantidad> listaProductoCantidadToUpdate = new List<ProductoCantidad>();
            //Aquellos productos que estan en la request pero no estan asignados en la promo, los agrego
            foreach (ProductoCantidad productoCantidad in request.ProductoCantidad)
            {
                if (!response.Result.Any(x => x.IdProducto == productoCantidad.IdProducto))
                {
                    listaProductoCantidadToAdd.Add(productoCantidad);
                }
                //Si ya esta el producto de la request asignado a la promo pero la cantidad de unidades es distinta, entonces se modifica
                if(response.Result.Any(x => x.IdProducto == productoCantidad.IdProducto && x.CantidadUnidades != productoCantidad.Cantidad))
                {
                    listaProductoCantidadToUpdate.Add(productoCantidad);
                }
            }
            //Si algun producto asignado a la promo no esta en la request, entonces los borro
            foreach (PromoProducto promoProducto in response.Result)
            {
                if (!request.ProductoCantidad.Select(x => x.IdProducto).Any(x => x == promoProducto.IdProducto))
                {
                    listaIdProductoToDelete.Add(promoProducto.IdProducto);
                }
            }
            if (listaProductoCantidadToAdd.Count > 0)
            {
                foreach (ProductoCantidad productoCantidad in listaProductoCantidadToAdd)
                {
                    PromoProducto promoProducto = new PromoProducto()
                    {
                        IdPromo = request.IdPromo,
                        IdProducto = productoCantidad.IdProducto,
                        CantidadUnidades = productoCantidad.Cantidad,
                        Activo = true,
                        FechaAlta = DateTime.Now
                    };
                    ServiceResponseResult<PromoProducto> responsePromoProducto = _promoProductoService.AgregarPromoProducto(promoProducto);
                    if (!responsePromoProducto.Success)
                    {
                        return BadRequest(responsePromoProducto.Mensage);
                    }

                }
            }
            if (listaIdProductoToDelete.Count > 0)
            {
                foreach (int id in listaIdProductoToDelete)
                {

                    ServiceResponseResult<PromoProducto> responsePromoProducto = _promoProductoService.QuitarProductoDePromo(id, request.IdPromo);
                    if (!responsePromoProducto.Success)
                    {
                        return BadRequest(responsePromoProducto.Mensage);
                    }

                }
            }
            if (listaProductoCantidadToUpdate.Count > 0)
            {
                foreach (ProductoCantidad productoCantidad in listaProductoCantidadToUpdate)
                {
                    PromoProducto promoProducto = response.Result.Where(x => x.IdProducto == productoCantidad.IdProducto).FirstOrDefault();

                    promoProducto.CantidadUnidades = productoCantidad.Cantidad;
                    ServiceResponseResult<PromoProducto> responsePromoProducto = _promoProductoService.ModificarPromoProducto(promoProducto);
                    if (!responsePromoProducto.Success)
                    {
                        return BadRequest(responsePromoProducto.Mensage);
                    }
                }
            }
            return Ok();

        }
    }
}