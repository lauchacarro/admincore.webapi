﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AdminCore.Business.Dto;
using AdminCore.Services.Abstract;
using AdminCore.Services.Configuration;
using AdminCore.WebApi.Models;
using AdminCore.WebApi.Requests.Auth;
using JwtAuthenticationHelper;
using JwtAuthenticationHelper.Abstractions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace JwtTokenAuthRefImplementation.API.Controllers
{
 
    [Route("api/auth")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IUsuarioService _usuarioService;

        private readonly IJwtTokenGenerator tokenGenerator;

        public AuthController(IUsuarioService usuarioService, IJwtTokenGenerator tokenGenerator)
        {
            // _configuration = configuration;
            _usuarioService = usuarioService;
         
            this.tokenGenerator = tokenGenerator;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromForm]LoginUsuarioPostRequest credenciales)
        {
            // Replace this with your custom authentication logic which will
            // securely return the authenticated user's details including
            // any role specific info

            var user = new Usuario
            {
                Alias = credenciales.Usuario,
                Contraseña = credenciales.Contraseña

            };
            if (_usuarioService.EsValido(ref user, out string msgError))
            {

                user.Contraseña = null;
                JwtAuthenticationHelper.Types.TokenWithClaimsPrincipal accessTokenResult = tokenGenerator.GenerateAccessTokenWithClaimsPrincipal(
                    credenciales.Usuario,
                    AddMyClaims(user));

                return Json(new { token = accessTokenResult.AccessToken });

            }
            else
            {
                return Unauthorized();
            }
        }

        private static IEnumerable<Claim> AddMyClaims(Usuario user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Actor, JsonConvert.SerializeObject(user)),
                new Claim(ClaimTypes.Role, user.Tipo.ToString()),
                new Claim(ClaimTypes.System, user.Proyecto.ToString())
            };

            return claims;
        }
    }

}